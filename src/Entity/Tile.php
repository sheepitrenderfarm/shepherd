<?php

namespace App\Entity;

use App\Repository\TileRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: TileRepository::class)]
#[ORM\Table(name: 'tile')]
class Tile implements \Stringable {
    public const STATUS_WAITING = 'waiting';
    public const STATUS_PROCESSING = 'rendering';
    public const STATUS_FINISHED = 'rendered';
    public const STATUS_PAUSED = 'paused';


    #[ORM\Id]
    #[ORM\Column(type: Types::STRING, length: 40)]
    private string $id;

    #[ORM\Column(type: Types::INTEGER)]
    #[Assert\NotBlank]
    private int $number;

    #[ORM\Column(type: Types::STRING)]
    #[Assert\NotBlank]
    private string $status;

    #[ORM\Column(type: Types::STRING, nullable: true)]
    private string $token;

    #[ORM\ManyToOne(targetEntity: Frame::class, inversedBy: 'tiles')]
    #[ORM\JoinColumn(nullable: false)]
    private Frame $frame;

    public function getId(): ?string {
        return $this->id;
    }

    public function setId(string $id): void {
        $this->id = $id;
    }

    public function getNumber(): ?int {
        return $this->number;
    }

    public function setNumber(int $number): self {
        $this->number = $number;

        return $this;
    }

    public function getStatus(): ?string {
        return $this->status;
    }

    public function setStatus(string $status): self {
        $this->status = $status;

        return $this;
    }

    public function getToken(): ?string {
        return $this->token;
    }

    public function setToken(?string $token): self {
        $this->token = $token;

        return $this;
    }

    public function getFrame(): Frame {
        return $this->frame;
    }

    public function setFrame(Frame $frame): self {
        $this->frame = $frame;

        return $this;
    }

    public function getImageExtension(): string {
        if ($this->frame->getType() == "full") {
            return $this->frame->getImageExtension();
        }
        else {
            return 'png';
        }
    }

    public function __toString(): string {
        return 'Tile(id: '.$this->id.' number: '.$this->number.' status: '.$this->status.' token: '.$this->token /* . ' startTime: ' . $this->startTime . ' validationTime: ' . $this->validationTime*/.')';
    }
}
