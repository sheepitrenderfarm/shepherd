<?php

namespace App\Entity;

use App\Repository\BlendRepository;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: BlendRepository::class)]
#[ORM\Table(name: 'blend')]
class Blend implements \Stringable {
    #[ORM\Id]
    #[ORM\Column(type: Types::INTEGER)]
    #[Assert\NotBlank]
    private int $id;

    #[ORM\Column(type: Types::FLOAT)]
    #[Assert\NotBlank]
    private float $framerate;

    #[ORM\Column(type: Types::INTEGER)]
    #[Assert\NotBlank]
    private int $width;

    #[ORM\Column(type: Types::INTEGER)]
    #[Assert\NotBlank]
    private int $height;

    #[ORM\Column(type: Types::BIGINT, nullable: false)]
    #[Assert\NotBlank]
    private int $size_tiles = 0;

    #[ORM\Column(type: Types::BIGINT, nullable: false)]
    #[Assert\NotBlank]
    private int $size_frames = 0;

    #[ORM\Column(type: Types::BIGINT, nullable: false)]
    #[Assert\NotBlank]
    private int $size_zip = 0;

    #[ORM\Column(type: Types::BIGINT, nullable: false)]
    #[Assert\NotBlank]
    private int $size_mp4s = 0;

    #[ORM\Column(type: Types::BIGINT, nullable: false)]
    #[Assert\NotBlank]
    private int $size_total_prediction = 0;

    /**
     * Generate mp4 videos
     */
    #[ORM\Column(type: Types::BOOLEAN, nullable: true)]
    private ?bool $generateMp4 = null;

    /**
     * @var Collection<int, Frame>
     */
    #[ORM\OneToMany(targetEntity: Frame::class, mappedBy: 'blend')]
    private Collection $frames;

    #[ORM\Column(type: Types::STRING, nullable: true)]
    private ?string $ownerToken = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTimeInterface $ownerTokenValidity = null;

    #[ORM\Column(type: Types::STRING, nullable: true)]
    private string $thumbnailToken;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTimeInterface $thumbnailTokenValidity = null;

    public function __construct() {
        $this->frames = new ArrayCollection();
    }

    public function __toString(): string {
        return 'Blend(id: '.$this->id.' framerate: '.$this->framerate.')';
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function setId(int $id): void {
        $this->id = $id;
    }

    public function getFramerate(): ?float {
        return $this->framerate;
    }

    public function setFramerate(float $framerate): self {
        $this->framerate = $framerate;

        return $this;
    }

    public function getWidth(): ?int {
        return $this->width;
    }

    public function setWidth(int $width): self {
        $this->width = $width;

        return $this;
    }

    public function getHeight(): ?int {
        return $this->height;
    }

    public function setHeight(int $height): self {
        $this->height = $height;

        return $this;
    }

    public function getSizeTiles(): int {
        return $this->size_tiles;
    }

    public function setSizeTiles(int $size): self {
        $this->size_tiles = $size;

        return $this;
    }

    public function getSizeFrames(): int {
        return $this->size_frames;
    }

    public function setSizeFrames(int $size): self {
        $this->size_frames = $size;

        return $this;
    }

    public function getSizeZIP(): int {
        return $this->size_zip;
    }

    public function setSizeZIP(int $size): self {
        $this->size_zip = $size;

        return $this;
    }

    public function getSizeMP4s(): int {
        return $this->size_mp4s;
    }

    public function setSizeMP4s(int $size): self {
        $this->size_mp4s = $size;

        return $this;
    }

    public function getPredictedTotalSize(): int {
        return $this->size_total_prediction;
    }

    public function setPredictedTotalSize(int $size): self {
        $this->size_total_prediction = $size;

        return $this;
    }
    
    /**
     * @return Collection|Frame[]
     */
    public function getFrames(): Collection {
        return $this->frames;
    }

    public function addFrame(Frame $frame): self {
        if (!$this->frames->contains($frame)) {
            $this->frames[] = $frame;
            $frame->setBlend($this);
        }

        return $this;
    }

    public function removeFrame(Frame $frame): self {
        if ($this->frames->contains($frame)) {
            $this->frames->removeElement($frame);

            // set the owning side to null (unless already changed)
            if ($frame->getBlend() === $this) {
                $frame->setBlend(null);
            }
        }

        return $this;
    }

    public function getOwnerToken(): ?string {
        return $this->ownerToken;
    }

    public function setOwnerToken(?string $ownerToken): self {
        $this->ownerToken = $ownerToken;

        return $this;
    }

    public function getOwnerTokenValidity(): ?\DateTimeInterface {
        return $this->ownerTokenValidity;
    }

    public function setOwnerTokenValidity(?\DateTimeInterface $ownerTokenValidity): self {
        $this->ownerTokenValidity = $ownerTokenValidity;

        return $this;
    }

    public function getThumbnailToken(): ?string {
        return $this->thumbnailToken;
    }

    public function setThumbnailToken(?string $thumbnailToken): self {
        $this->thumbnailToken = $thumbnailToken;

        return $this;
    }

    public function getThumbnailTokenValidity(): ?\DateTimeInterface {
        return $this->thumbnailTokenValidity;
    }

    public function setThumbnailTokenValidity(?\DateTimeInterface $thumbnailTokenValidity): self {
        $this->thumbnailTokenValidity = $thumbnailTokenValidity;

        return $this;
    }

    public function getGenerateMp4(): ?bool {
        return $this->generateMp4;
    }

    public function setGenerateMp4(?bool $generateMp4): self {
        $this->generateMp4 = $generateMp4;

        return $this;
    }

    public function getSize(): int {
        return $this->getSizeTiles() + $this->getSizeFrames() + $this->getSizeZIP() + $this->getSizeMP4s();
    }
}
