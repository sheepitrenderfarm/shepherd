<?php

namespace App\Entity;

use App\Repository\TaskRepository;
use DateTimeInterface;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: TaskRepository::class)]
#[ORM\Table(name: 'task')]
#[ORM\Index(columns: ['status'])]
#[ORM\Index(columns: ['type'])]
class Task implements \Stringable {
    public const TYPE_GENERATE_TILE_THUMBNAIL = 'generate_thumbnail_tile';
    public const TYPE_GENERATE_FRAME_THUMBNAIL = 'generate_thumbnail';
    public const TYPE_GENERATE_FRAME = 'generate_frame';
    public const TYPE_GENERATE_MP4_FINAL = 'genreate_mp4_final';
    public const TYPE_GENERATE_MP4_PREVIEW = 'generate_mp4_preview';
    public const TYPE_GENERATE_ZIP = 'generate_zip';
    public const TYPE_DELETE_BLEND = 'delete_blend';
    public const TYPE_VALIDATE_TILE = 'validate_tile';
    public const TYPE_NOTIFY_ZIP = 'notify_zip';
    public const TYPE_NOTIFY_MP4_FINAL = 'notify_mp4_final';
    public const TYPE_NOTIFY_MP4_PREVIEW = 'notify_mp4_preview';

    public const STATUS_RUNNING = 'running';
    public const STATUS_WAITING = 'waiting';

    public const MAX_RUNNING_TIME = 1800; // in second
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: Types::INTEGER)]
    #[Assert\NotBlank]
    private int $id;// @phpstan-ignore-line because the id is not used on php side but only sql side

    #[ORM\Column(type: Types::STRING)]
    #[Assert\NotBlank]
    private string $type;

    #[ORM\Column(type: Types::STRING, nullable: false)]
    #[Assert\NotBlank]
    private string $status = 'waiting';

    #[ORM\ManyToOne(targetEntity: Blend::class)]
    #[ORM\JoinColumn(nullable: false)]
    private Blend $blend;

    #[ORM\ManyToOne(targetEntity: Frame::class)]
    #[ORM\JoinColumn(nullable: true)]
    private ?Frame $frame = null;

    #[ORM\ManyToOne(targetEntity: Tile::class)]
    #[ORM\JoinColumn(nullable: true)]
    private ?Tile $tile = null;

    /**
     * @var array<string, string>
     */
    #[ORM\Column(name: 'data', type: Types::JSON, nullable: false)]
    private array $data = [];

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTimeInterface $startTime = null;

    #[ORM\Column(type: Types::INTEGER)]
    private int $pid = -1;

    public function getId(): ?int {
        return $this->id;
    }

    public function getType(): ?string {
        return $this->type;
    }

    public function setType(string $type): self {
        $this->type = $type;

        return $this;
    }

    public function getBlend(): Blend {
        return $this->blend;
    }

    public function setBlend(Blend $blend): self {
        $this->blend = $blend;

        return $this;
    }

    public function getFrame(): ?Frame {
        return $this->frame;
    }

    public function setFrame(?Frame $frame): self {
        $this->frame = $frame;

        return $this;
    }

    public function getStatus(): string {
        return $this->status;
    }

    public function setStatus(string $status): self {
        $this->status = $status;

        return $this;
    }

    public function getTile(): ?Tile {
        return $this->tile;
    }

    public function setTile(?Tile $tile): self {
        $this->tile = $tile;

        return $this;
    }

    /**
     * @return array<string, string>
     */
    public function getData(): array {
        return $this->data;
    }

    public function setData(array $data): self {
        $this->data = $data;

        return $this;
    }

    public function getStartTime(): ?DateTimeInterface {
        return $this->startTime;
    }

    public function setStartTime(?DateTimeInterface $startTime): self {
        $this->startTime = $startTime;

        return $this;
    }

    public function getPID(): int {
        return $this->pid;
    }

    public function setPID(int $pid): self {
        $this->pid = $pid;

        return $this;
    }

    public function __toString(): string {
        return 'Task(id: '.$this->id.' type: '.$this->type.' frame: '.(is_null($this->frame) ? 'isNull' : $this->frame->getId()).' blend: '.$this->blend->getId().')';
    }

    public function isNotify(): bool {
        return $this->type == self::TYPE_NOTIFY_ZIP || $this->type == self::TYPE_NOTIFY_MP4_PREVIEW || $this->type == self::TYPE_NOTIFY_MP4_FINAL;
    }
}
