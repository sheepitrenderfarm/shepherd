<?php

namespace App\Command;

use App\Repository\BlendRepository;
use App\Service\BlendService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'shepherd:checker:directory',
    description: 'Create all missing blend directories'
)]
class DirectoryCheckerCommand extends Command {
    public function __construct(
        private BlendRepository $blendRepository,
        private BlendService $blendService,
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int {
        foreach ($this->blendRepository->findAll() as $blend) {
            $this->blendService->createDirectories($blend);
        }

        return Command::SUCCESS;
    }
}
