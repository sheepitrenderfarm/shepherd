<?php

namespace App\Command;

use App\Entity\Task;
use App\Monitoring\MonitoringCpu;
use App\Repository\TaskRepository;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Lock\LockFactory;
use Symfony\Component\Lock\Store\FlockStore;

#[AsCommand(
    name: 'shepherd:debug',
    description: 'Show debug stats'
)]
class DebugCommand extends Command {
    public function __construct(
        private ContainerBagInterface $containerBag,
        private TaskRepository $taskRepository
    ) {
        parent::__construct();
    }

    /**
     * This method is executed after initialize(). It usually contains the logic
     * to execute to complete this command task.
     */
    protected function execute(InputInterface $input, OutputInterface $output): int {
        echo "----------- CPU -----------\n";
        echo "load        ".(new MonitoringCpu())->getHumanValue()."\n";


        /**
         * @var FlockStore
         */
        $store = new FlockStore(sys_get_temp_dir());
        /**
         * @var LockFactory
         */
        $lockFactory = new LockFactory($store);

        // same code as TasksExecuteCommand: TODO: do not duplicate code...
        if (intval($this->containerBag->get('concurrent_tasks')) != 0) {
            $concurrent_tasks = intval($this->containerBag->get('concurrent_tasks'));
        }
        else {
            $concurrent_tasks = intval(rtrim(shell_exec('nproc')));
        }

        echo "------- Runners max: {$concurrent_tasks} ---------\n";

        for ($i = 1; $i <= $concurrent_tasks; $i++) {
            $lock = $lockFactory->createLock("shepherd.task.$i.pid");
            $lock->acquire(false);
            echo sprintf("%2d => %s", $i, $lock->isAcquired() ? 'Free' : 'Acquired');

            if ($lock->isAcquired()) {
                $lock->release();
            }

            echo "\n";
        }

        echo "----------- Tasks --------\n";
        echo "total: ".number_format($this->taskRepository->count([]))."\n";
        echo "distinct blends: ".number_format(count($this->taskRepository->getDistinctBlends()))."\n";
        echo "running blends: ".number_format(count($this->taskRepository->getDistinctRunningBlends()))."\n";
        echo "Running tasks:  ".number_format($this->taskRepository->count(['status' => Task::STATUS_RUNNING]))."\n";

        foreach ($this->taskRepository->findBy(['status' => Task::STATUS_RUNNING]) as $t) {
            /** @var Task $t */
            echo "task: ".$t->getId()." pid: ".$t->getPID()." blend: ".$t->getBlend()->getId()." type: ".$t->getType()."\n";
        }

        echo "--------------------------\n";


        return Command::SUCCESS;
    }
}
