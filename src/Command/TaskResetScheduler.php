<?php

namespace App\Command;

use App\Repository\TaskRepository;
use App\Service\MasterService;
use App\Service\TaskService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * A console command that resets all timed out tasks.
 *
 *     $ php bin/console shepherd:resettasks
 *
 */
#[AsCommand(
    name: 'shepherd:resettasks',
    description: 'Resets all timed out tasks'
)]
class TaskResetScheduler extends Command {
    public function __construct(
        private EntityManagerInterface $entityManager,
        private MasterService $masterService,
        private TaskRepository $taskRepository,
        private TaskService $taskService
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int {
        foreach ($this->taskRepository->findDeadTasks() as $task) {
            $this->taskService->reset($task);
            $this->masterService->notifyTaskReset($task);
        }

        $this->entityManager->flush();

        return Command::SUCCESS;
    }
}
