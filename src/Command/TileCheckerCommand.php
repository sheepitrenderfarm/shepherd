<?php

namespace App\Command;

use App\Entity\Tile;
use App\Repository\TileRepository;
use App\Service\MasterService;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'shepherd:checker:tile',
    description: 'Check if all tiles are correct'
)]
class TileCheckerCommand extends Command {
    public function __construct(
        private TileRepository $tileRepository,
        private MasterService $masterService,
        private LoggerInterface $logger
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int {
        foreach ($this->tileRepository->findBy(['status' => Tile::STATUS_FINISHED]) as $tile) {
            /** @var Tile $tile */
            if (file_exists($this->tileRepository->getPath($tile)) == false) {
                $this->logger->error(__method__."tile is finished but file doesn't exist ".$this->tileRepository->getPath($tile));
                $this->masterService->notifyTileFailed($tile);
            }
        }

        return Command::SUCCESS;
    }
}
