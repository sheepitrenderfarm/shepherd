<?php

namespace App\Command;

use App\Tool\NetworkUsage;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * A console command that save the current network usage
 */
#[AsCommand(
    name: 'shepherd:monitoring',
    description: 'Generate monitoring value'
)]
class TasksMonitoringCommand extends Command {
    /**
     * This method is executed after initialize(). It usually contains the logic
     * to execute to complete this command task.
     */
    protected function execute(InputInterface $input, OutputInterface $output): int {
        (new NetworkUsage())->generate();

        return Command::SUCCESS;
    }
}
