<?php

namespace App\Command;

use App\Repository\BlendRepository;
use App\Service\BlendService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Update storage used by the blends
 *
 *     $ php bin/console shepherd:update-storage-usage
 *
 */
#[AsCommand(
    name: 'shepherd:update-storage-usage',
    description: 'Update storage used by blends'
)]
class UpdateStorageCommand extends Command {
    public function __construct(
        private BlendRepository $blendRepository,
        private BlendService $blendService
    ) {
        parent::__construct();
    }

    /**
     * This method is executed after initialize(). It usually contains the logic
     * to execute to complete this command task.
     */
    protected function execute(InputInterface $input, OutputInterface $output): int {
        foreach ($this->blendRepository->findAll() as $blend) {
            $this->blendService->setSize($blend);
        }

        return Command::SUCCESS;
    }
}
