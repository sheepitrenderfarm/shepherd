<?php

namespace App\Command;

use App\Entity\Task;
use App\Monitoring\MonitoringCpu;
use App\Repository\TaskRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Lock\LockFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Lock\LockInterface;
use Symfony\Component\Lock\Store\FlockStore;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Process\PhpExecutableFinder;

/**
 * A console command that execute waiting task.
 *
 *     $ php bin/console app:task
 *
 */
#[AsCommand(
    name: 'shepherd:tasks',
    description: 'Execute all async tasks (zip, mp4 generation, ...)'
)]
class TasksExecuteCommand extends Command {
    protected const MAX_EXECUTION_TIME = 1800; // in seconds
    private ?LockInterface $lockWorker = null;
    private LockFactory $lockFactory;

    private DateTime $start;

    public function __construct(
        private ContainerBagInterface $containerBag,
        private EntityManagerInterface $entityManager,
        private TaskRepository $taskRepository,
        private LoggerInterface $logger
    ) {
        parent::__construct();

        $store = new FlockStore(sys_get_temp_dir());
        $this->lockFactory = new LockFactory($store);

        $this->start = new DateTime();
    }

    /**
     * This method is executed after initialize(). It usually contains the logic
     * to execute to complete this command task.
     */
    protected function execute(InputInterface $input, OutputInterface $output): int {
        if ($this->canWork()) {
            do {
                $status = $this->workOneTime();
            }
            while ($status == true && $this->stillHaveTime());
        }

        $this->shutdown();

        return Command::SUCCESS;
    }

    /**
     * @return bool|Task true -> no task, false -> can not find task, object -> found a task
     */
    private function nextTask(): bool|Task {
        if ($this->isCPUOverloaded()) {
            return false;
        }

        $lock = $this->lockFactory->createLock('shepherd.task.search');

        $ret = false;

        $jobfindattempts = 0;
        $jobfindattemptsmax = 10;

        while ($ret == false && $jobfindattempts <= $jobfindattemptsmax) {
            if ($lock->acquire(true)) {
                $ret = $this->nextTasksActual();
                $lock->release();

                if (is_object($ret)) {
                    return $ret;
                }
            }

            sleep(rand(1, 3));
            $jobfindattempts++;
        }

        return $ret;
    }

    /**
     * @return bool|Task true -> no task, false -> can not find task, object -> found a task
     */
    private function nextTasksActual(): bool|Task {
        if ($this->taskRepository->count([]) == 0) {
            return true;
        }

        /** @var ?Task $task */
        $task = $this->taskRepository->findOneAvailable();

        if (is_object($task)) {
            $task->setStatus(Task::STATUS_RUNNING);
            $task->setStartTime(new DateTime());
            $this->entityManager->flush();
            return $task;
        }
        else {
            return false;
        }
    }

    private function workOneTime(): bool {
        $task = $this->nextTask();

        if ($task === true) { // no job
            return false;
        }
        elseif ($task === false) { // error
            return false;
        }
        else {
            $this->executeTask($task);
            return true; // silent the error for $this->work()
        }
    }

    private function executeTask(Task $task): bool {
        $this->logger->debug(__method__." ".$task->getId());

        $phpBinaryFinder = new PhpExecutableFinder();
        $phpBinaryPath = $phpBinaryFinder->find();

        $cmd = $phpBinaryPath.' '.__DIR__.'/../../bin/console '.TaskSingleExecuteCommand::getDefaultName().' '.$task->getId();
        exec($cmd);

        return true;
    }

    private function canWork(): bool {
        $concurrent_workers = $this->getMaxWorkerCount();

        for ($i = 1; $i <= $concurrent_workers; $i++) {
            $lock = $this->lockFactory->createLock("shepherd.task.$i.pid");

            if ($lock->acquire(false)) {
                $this->lockWorker = $lock;
                return true;
            }
        }

        return false;
    }

    /**
     * Only works for 30min to avoid any big memory leak
     */
    private function stillHaveTime(): bool {
        return (new DateTime())->getTimestamp() - $this->start->getTimestamp() < self::MAX_EXECUTION_TIME;
    }

    private function shutdown(): void {
        if (is_null($this->lockWorker) == false) {
            $this->lockWorker->release();
        }
    }

    /**
     * Is the system is too loaded, do not work
     */
    private function isCPUOverloaded(): bool {
        // sys_load / worker count = worker_load
        // sys_load_max / worker_load = amount of workers allowed.
        $monitoring = new MonitoringCpu();

        $cpu_load_current = max($monitoring->getValue(), 1.0);
        $cpu_load_max = $monitoring->getMax();
        $concurrent_workers_current = (float)$this->getWorkerCount();
        $concurrent_workers_max = (float)$this->getMaxWorkerCount();

        return $cpu_load_max / ($cpu_load_current / $concurrent_workers_max) < $concurrent_workers_current;
    }

    private function getWorkerCount(): int {
        $concurrent_workers = $this->getMaxWorkerCount();

        $count = 0;

        for ($i = 1; $i <= $concurrent_workers; $i++) {
            $lock = $this->lockFactory->createLock("shepherd.task.$i.pid");

            if ($lock->isAcquired()) {
                $count++;
            }
        }

        return $count;
    }

    private function getMaxWorkerCount(): int {
        if (intval($this->containerBag->get('concurrent_tasks')) != 0) {
            return intval($this->containerBag->get('concurrent_tasks'));
        }
        else {
            return intval(rtrim(shell_exec('nproc')));
        }
    }
}
