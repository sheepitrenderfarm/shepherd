<?php

namespace App\Command;

use App\Entity\Task;
use App\Repository\TaskRepository;
use App\Service\TaskService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * A console command that execute a single task.
 *
 *     $ php bin/console shepherd:task 1234
 *
 */
#[AsCommand(
    name: 'shepherd:singletask',
    description: 'Execute a task'
)]
class TaskSingleExecuteCommand extends Command {
    public function __construct(
        private EntityManagerInterface $entityManager,
        private TaskRepository $taskRepository,
        private TaskService $taskService
    ) {
        parent::__construct();
    }

    protected function configure(): void {
        $this->addArgument('id', InputArgument::REQUIRED, 'Task id');
    }

    /**
     * This method is executed after initialize(). It usually contains the logic
     * to execute to complete this command task.
     */
    protected function execute(InputInterface $input, OutputInterface $output): int {

        /** @var ?Task $task */
        $task = $this->taskRepository->find($input->getArgument('id'));

        if (is_object($task)) {
            $task->setStatus(Task::STATUS_RUNNING);
            $task->setPID(getmypid());
            $this->entityManager->flush();

            $ret = $this->taskService->execute($task);

            // for now even if the task failed, remove it.
            // TODO: maybe add it back ??

            $this->entityManager->remove($task);
            $this->entityManager->flush();

            return $ret ? Command::SUCCESS : Command::FAILURE;
        }
        else {
            echo "Failed to load task id: ".$input->getArgument('id')."\n";
            return Command::FAILURE;
        }
    }
}
