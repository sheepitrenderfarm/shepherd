<?php

namespace App;

use App\Tool\Size;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class TwigExtension extends AbstractExtension {
    public function getFunctions(): array {
        return [
            new TwigFunction('humanSize', $this->humanSize(...)),
        ];
    }

    public function humanSize(int $input): string {
        return Size::humanSize($input);
    }
}