<?php

namespace App\Repository;

use App\Entity\Blend;
use App\Entity\Tile;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;

class BlendRepository extends ServiceEntityRepository {
    public function __construct(ManagerRegistry $registry, private ContainerBagInterface $containerBag, private FrameRepository $frameRepository, private TileRepository $tileRepository) {
        parent::__construct($registry, Blend::class);
    }

    public function isFinished(Blend $blend): bool {
        foreach ($blend->getFrames() as $frame) {
            if ($this->frameRepository->isFinished($frame) == false) {
                return false;
            }
        }

        return true;
    }

    public function getStorageDirectory(Blend $blend): string {
        return $this->containerBag->get('storage_dir') /*. DIRECTORY_SEPARATOR*/.$blend->getId();
    }

    public function getMP4FinalPath(Blend $blend): string {
        return $this->getStorageDirectory($blend).DIRECTORY_SEPARATOR.'output'.DIRECTORY_SEPARATOR.$blend->getId().'_final.mp4';
    }

    public function getMP4PreviewPath(Blend $blend): string {
        return $this->getStorageDirectory($blend).DIRECTORY_SEPARATOR.'output'.DIRECTORY_SEPARATOR.$blend->getId().'_preview.mp4';
    }

    public function getZip(Blend $blend): string {
        return $this->getStorageDirectory($blend).DIRECTORY_SEPARATOR.'output'.DIRECTORY_SEPARATOR.$blend->getId().'.zip';
    }

    public function updatePredictedTotalSize(Blend $blend): void {
        $finished_tiles = 0;
        $tiles = $this->tileRepository->getTiles($blend);

        foreach ($tiles as $tile) {
            if ($tile->getStatus() == Tile::STATUS_FINISHED) {
                $finished_tiles++;
            }
        }

        if ($finished_tiles != 0) {
            $usage_tiles = $blend->getSizeTiles() * (float)count($tiles) / (float)$finished_tiles;

            $frame = $blend->getFrames()->first();

            if ($frame->getType() == 'full' || $frame->getTiles()->count() == 1 || $frame->getType() == 'layer') {
                // on 10 layers -> frame disk is 1 tile
                $usage_frames = $usage_tiles / $frame->getTiles()->count();
            }
            else {
                // on 4x4 chessboard/region -> frame disk is 16 tiles
                $usage_frames = $usage_tiles;
            }

            $usage_zip = $usage_frames; // we zip only the frame
            $usage_mp4 = 0; // no data for final or preview :(

            $blend->setPredictedTotalSize($usage_tiles + $usage_frames + $usage_zip + $usage_mp4);
        }
        else {
            $blend->setPredictedTotalSize(0);
        }
    }
}
