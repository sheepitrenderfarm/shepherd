<?php

namespace App\Repository;

use App\Entity\Blend;
use App\Entity\Task;
use App\Entity\Tile;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;

class TileRepository extends ServiceEntityRepository {
    public function __construct(ManagerRegistry $registry, private ContainerBagInterface $containerBag) {
        parent::__construct($registry, Tile::class);
    }

    /**
     * @return Tile[]
     */
    public function getTiles(Blend $blend): array {
        return array_merge(...array_map(function ($frame) { return $frame->getTiles()->toArray(); }, $blend->getFrames()->toArray()));
    }

    public function isFinished(Tile $tile): bool {
        if ($tile->getStatus() != Tile::STATUS_FINISHED) {
            return false;
        }

        return true;
    }

    public function getStorageDirectory(Tile $tile): string {
        return $this->containerBag->get('storage_dir')/* . DIRECTORY_SEPARATOR*/.$tile->getFrame()->getBlend()->getId().DIRECTORY_SEPARATOR.'tiles'.DIRECTORY_SEPARATOR.'frames'.DIRECTORY_SEPARATOR.$tile->getFrame()->getId();
    }

    public function getThumbnailPath(Tile $tile): string {
        return $this->containerBag->get('storage_dir').$tile->getFrame()->getBlend()->getId().DIRECTORY_SEPARATOR.'thumbnails'.DIRECTORY_SEPARATOR.'tiles'.DIRECTORY_SEPARATOR.$tile->getId().'.png';
    }

    public function getPath(Tile $tile): string {
        return $this->getStorageDirectory($tile).DIRECTORY_SEPARATOR.$tile->getId().'.'.$tile->getImageExtension();
    }

    public function reset(Tile $tile): void {
        $tile->setStatus(Tile::STATUS_WAITING);

        // also remove tasks link to the tile
        $taskrepository = $this->_em->getRepository(Task::class);

        $task1 = $taskrepository->findOneBy(['tile' => $tile, 'type' => Task::TYPE_GENERATE_TILE_THUMBNAIL]);

        if (is_object($task1)) {
            $this->_em->remove($task1);
        }

        $task2 = $taskrepository->findOneBy(['frame' => $tile->getFrame(), 'type' => Task::TYPE_GENERATE_FRAME_THUMBNAIL]);

        if (is_object($task2)) {
            $this->_em->remove($task2);
        }

        $task3 = $taskrepository->findOneBy(['frame' => $tile->getFrame(), 'type' => Task::TYPE_GENERATE_FRAME]);

        if (is_object($task3)) {
            $this->_em->remove($task3);
        }
    }
}
