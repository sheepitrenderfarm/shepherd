<?php

namespace App\Repository;

use App\Entity\Frame;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;

class FrameRepository extends ServiceEntityRepository {
    public function __construct(ManagerRegistry $registry, private ContainerBagInterface $containerBag, private TileRepository $tileRepository) {
        parent::__construct($registry, Frame::class);
    }

    public function isFinished(Frame $frame): bool {
        foreach ($frame->getTiles() as $tile) {
            if ($this->tileRepository->isFinished($tile) == false) {
                return false;
            }
        }

        return true;
    }

    public function getFullPath(frame $frame): string {
        return $this->getStorageDirectory($frame).DIRECTORY_SEPARATOR.sprintf("%04d", $frame->getNumber()).'.'.$frame->getImageExtension();
    }

    public function getThumbnailPath(Frame $frame): string {
        return $this->containerBag->get('storage_dir').$frame->getBlend()->getId().DIRECTORY_SEPARATOR.'thumbnails'.DIRECTORY_SEPARATOR.'frames'.DIRECTORY_SEPARATOR.sprintf("%04d", $frame->getNumber()).'.png';
    }

    public function getStorageDirectory(Frame $frame): string {
        return $this->containerBag->get('storage_dir') /*. DIRECTORY_SEPARATOR*/.$frame->getBlend()->getId().DIRECTORY_SEPARATOR.'frames';
    }
}
