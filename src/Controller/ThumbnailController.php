<?php

namespace App\Controller;

use App\Entity\Blend;
use App\Entity\Task;
use App\Entity\Tile;
use App\Monitoring\MonitoringCpu;
use App\Repository\FrameRepository;
use App\Repository\TaskRepository;
use App\Repository\TileRepository;
use App\Service\BlendService;
use App\Service\FrameService;
use App\Service\ThumbnailService;
use App\Service\TileService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller to handle request for thumbnail
 */
#[Route(path: '/thumb')]
class ThumbnailController extends AbstractController {
    public function __construct(
        private ContainerBagInterface $containerBag,
        private EntityManagerInterface $entityManager,
        private MonitoringCpu $monitoringCpu,
        private BlendService $blendService,
        private TileService $tileService,
        private FrameService $frameService,
        private FrameRepository $frameRepository,
        private TaskRepository $taskRepository,
        private TileRepository $tileRepository
    ) {
    }

    #[Route(path: '/{token}/{blend}/frame/{frameNumber}/thumbnail', methods: 'GET')]
    public function getFrameThumbnail(string $token, Blend $blend, int $frameNumber): BinaryFileResponse {
        if ($this->blendService->isThumbnailTokenValid($blend, $token) == false) {
            throw $this->createNotFoundException('token not valid');
        }

        $frame = $this->frameRepository->findOneBy(['blend' => $blend->getId(), 'number' => $frameNumber]);

        if (is_object($frame)) {
            $path = $this->frameRepository->getThumbnailPath($frame);

            if (file_exists($path) == false) {
                if ($this->containerBag->get('frame_thumbnail') == ThumbnailService::GENERATE_ON_DEMAND) {
                    $this->frameService->generateImageThumbnail($frame);
                }
                else {
                    throw $this->createNotFoundException('file not found');
                }
            }

            return $this->giveFile($path);
        }
        else {
            throw $this->createNotFoundException('file not found');
        }
    }

    #[Route(path: '/{token}/{blend}/tile/{tile}/thumbnail', methods: 'GET')]
    public function getTileThumbnail(string $token, Blend $blend, Tile $tile): BinaryFileResponse {
        return $this->getTileThumbnailOnDemand($token, $blend, $tile, $this->containerBag->get('tile_thumbnail') == ThumbnailService::GENERATE_ON_DEMAND);
    }

    #[Route(path: '/{token}/{blend}/tile/{tile}/thumbnail/{ondemand}', methods: 'GET')]
    public function getTileThumbnailOnDemand(string $token, Blend $blend, Tile $tile, bool $ondemand): BinaryFileResponse {
        if ($this->blendService->isThumbnailTokenValid($blend, $token) == false) {
            throw $this->createNotFoundException('token not valid');
        }

        $path = $this->tileRepository->getThumbnailPath($tile);

        if (file_exists($path) == false) {
            if ($ondemand && $this->monitoringCpu->isOverloaded() == false) {
                // don't exist -> create it and remove the waiting creation task
                if ($this->tileService->generateThumbnail($tile)) {
                    $task = $this->taskRepository->findOneBy(['tile' => $tile, 'type' => Task::TYPE_GENERATE_TILE_THUMBNAIL]);

                    if (is_object($task)) {
                        $this->entityManager->remove($task);
                        $this->entityManager->flush();
                    }
                }
                else {
                    throw $this->createNotFoundException('file not found');
                }
            }
            else {
                throw $this->createNotFoundException('file not found');
            }
        }

        return $this->giveFile($path);
    }

    private function giveFile(string $path): BinaryFileResponse {
        try {
            $file = new File($path, true);
        }
        catch (FileNotFoundException) {
            throw $this->createNotFoundException('file not found');
        }

        header('Content-Type: '.$file->getMimeType());
        header('Content-Length: '.filesize($file));

        return $this->file($file, $file->getFilename(), ResponseHeaderBag::DISPOSITION_INLINE);
    }
}
