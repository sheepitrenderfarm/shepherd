<?php

namespace App\Controller;

use App\Entity\Blend;
use App\Entity\Task;
use App\Entity\Tile;
use App\Monitoring\MonitoringCpu;
use App\Monitoring\MonitoringCpuPsi;
use App\Monitoring\MonitoringRamPsi;
use App\Monitoring\MonitoringDiskPsi;
use App\Monitoring\MonitoringDisk;
use App\Monitoring\MonitoringHttpd;
use App\Monitoring\MonitoringNetwork;
use App\Monitoring\MonitoringRam;
use App\Monitoring\MonitoringTask;
use App\Repository\BlendRepository;
use App\Repository\FrameRepository;
use App\Repository\TaskRepository;
use App\Repository\TileRepository;
use App\Service\BlendService;
use App\Tool\NetworkUsage;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\HttpFoundation\IpUtils;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Process\PhpExecutableFinder;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller to handle request from Master server
 */
#[Route(path: '/master')]
class MasterController {
    public function __construct(
        private EntityManagerInterface $entityManager,
        private ContainerBagInterface $containerBag,
        private BlendService $blendService,
        private BlendRepository $blendRepository,
        private FrameRepository $frameRepository,
        private TileRepository $tileRepository, private TaskRepository $taskRepository
    ) {
    }

    /**
     * Add a blend to the shepherd
     */
    #[Route(path: '/blend', methods: 'POST')]
    public function addBlend(Request $request): Response {
        if ($this->isComingFromMaster($request) == false) {
            return new Response('', 403);
        }

        $data = json_decode($request->getContent(), true);

        if (
            array_key_exists('blend', $data) == false ||
            array_key_exists('framerate', $data) == false ||
            array_key_exists('width', $data) == false ||
            array_key_exists('height', $data) == false ||
            array_key_exists('mp4', $data) == false ||
            array_key_exists('image_extension', $data) == false ||
            array_key_exists('frames', $data) == false
        ) {
            return new Response('', 400);
        }

        // optional: tokens
        $tokenOwner = null;
        $tokenOwnerValidity = null;
        $tokenThumbnail = null;
        $tokenThumbnailValidity = null;

        if (array_key_exists('token_owner', $data) && array_key_exists('token_owner_validity', $data)) {
            $tokenOwner = $data['token_owner'];
            $tokenOwnerValidity = new DateTime();
            $tokenOwnerValidity->setTimestamp($data['token_owner_validity']);
        }

        if (array_key_exists('token_thumbnail', $data) && array_key_exists('token_thumbnail_validity', $data)) {
            $tokenThumbnail = $data['token_thumbnail'];
            $tokenThumbnailValidity = new DateTime();
            $tokenThumbnailValidity->setTimestamp($data['token_thumbnail_validity']);
        }

        if ($this->blendService->addBlend($data['blend'], $data['framerate'], $data['width'], $data['height'], $data['mp4'], $data['frames'], $tokenOwner, $tokenOwnerValidity, $tokenThumbnail, $tokenThumbnailValidity)) {
            return new Response('', 200);
        }
        else {
            return new Response('', 500);
        }
    }

    /**
     * Remove a blend to the shepherd
     */
    #[Route(path: '/blend/{blend}/remove', methods: 'GET')]
    public function delBlend(Request $request, Blend $blend): Response {
        if ($this->isComingFromMaster($request) == false) {
            return new Response('', 403);
        }

        // do not directly delete the blend because it will take a lot of time
        $taskZip = new Task();
        $taskZip->setType(Task::TYPE_DELETE_BLEND);
        $taskZip->setBlend($blend);

        // remove the project's tasks to avoid working on 'zombie' tasks
        foreach ($this->taskRepository->findBy(['blend' => $blend->getId()]) as $task) {
            $this->entityManager->remove($task);
        }

        $this->entityManager->persist($taskZip);
        $this->entityManager->flush();

        return new Response('', 200);
    }

    /**
     * Check artefacts of a blend (tiles, frames, zip, mp4s)
     */
    #[Route(path: '/blend/{blend}/check', methods: 'GET')]
    public function checkBlend(Request $request, Blend $blend): Response {
        if ($this->isComingFromMaster($request) == false) {
            return new Response('', 403);
        }

        $ret = ['tile' => [], 'frame' => [], 'zip' => [], 'mp4' => []];

        foreach ($blend->getFrames() as $frame) {
            foreach ($frame->getTiles() as $tile) {
                if ($tile->getStatus() == Tile::STATUS_FINISHED && file_exists($this->tileRepository->getPath($tile)) == false) {
                    $ret['tile'] [] = $tile->getId();
                }
            }

            if (file_exists($this->frameRepository->getFullPath($frame)) == false && is_null($this->taskRepository->findOneBy(['frame' => $frame->getId(), 'type' => Task::TYPE_GENERATE_FRAME]))) {
                $ret['frame'] [] = $frame->getId();
            }
        }

        $mp4final = $this->blendRepository->getMP4FinalPath($blend);

        if ($blend->getGenerateMp4() && file_exists($mp4final) == false && is_null($this->taskRepository->findOneBy(['blend' => $blend->getId(), 'type' => Task::TYPE_GENERATE_MP4_FINAL]))) {
            $ret['mp4'] [] = 'final';
        }

        $mp4preview = $this->blendRepository->getMP4PreviewPath($blend);

        if ($blend->getGenerateMp4() && file_exists($mp4preview) == false && is_null($this->taskRepository->findOneBy(['blend' => $blend->getId(), 'type' => Task::TYPE_GENERATE_MP4_PREVIEW]))) {
            $ret['mp4'] [] = 'preview';
        }

        $zip = $this->blendRepository->getZip($blend);

        if (file_exists($zip) == false && is_null($this->taskRepository->findOneBy(['blend' => $blend->getId(), 'type' => Task::TYPE_GENERATE_ZIP]))) {
            $ret['zip'] [] = 'zip';
        }

        return new JsonResponse($ret, 200);
    }

    /**
     * Get blends infos
     */
    #[Route(path: '/blend/list', methods: 'GET')]
    public function getBlendList(Request $request): JsonResponse {
        if ($this->isComingFromMaster($request) == false) {
            return new JsonResponse($request->getClientIp(), 403);
        }

        $data = [];

        foreach ($this->blendRepository->findAll() as $k => $blend) {
            /** @var Blend $blend */
            $data [] = ['id' => $blend->getId(), 'size' => $blend->getSize(), 'size_prediction' => $blend->getPredictedTotalSize()];
        }

        return new JsonResponse($data, 200);
    }

    /**
     * Get storage usage for a blend
     */
    #[Route(path: '/blend/{blend}/storage', methods: 'GET')]
    public function storageUsageBlend(Request $request, Blend $blend): Response {
        if ($this->isComingFromMaster($request) == false) {
            return new Response('', 403);
        }

        return new Response(strval($blend->getSize()), 200);
    }


    /**
     * Get storage prediction for a blend
     */
    #[Route(path: '/blend/{blend}/storage_prediction', methods: 'GET')]
    public function storagePredictionBlend(Request $request, Blend $blend): Response {
        if ($this->isComingFromMaster($request) == false) {
            return new Response('', 403);
        }

        return new Response(strval($blend->getPredictedTotalSize()), 200);
    }

    /**
     * set a rendering tile
     */
    #[Route(path: '/tile/{uid}/rendering/{token}', methods: 'GET')]
    public function tileRendering(Request $request, string $uid, string $token): Response {
        if ($this->isComingFromMaster($request) == false) {
            return new Response('', 403);
        }

        $tile = $this->tileRepository->find($uid);

        if (is_null($tile)) {
            return new Response('', 404);
        }

        $tile->setStatus(Tile::STATUS_PROCESSING);
        $tile->setToken($token);

        $this->entityManager->flush();

        return new Response('', 200);
    }

    /**
     * reset a tile
     */
    #[Route(path: '/tile/{uid}/reset', methods: 'GET')]
    public function tileReset(Request $request, string $uid): Response {
        if ($this->isComingFromMaster($request) == false) {
            return new Response('', 403);
        }

        /** @var ?Tile $tile */
        $tile = $this->tileRepository->find($uid);

        if (is_null($tile)) {
            return new Response('', 404);
        }

        $this->tileRepository->reset($tile);

        $this->entityManager->flush();

        // remove tile, frame, mp4, zip file too
        $tilePath = $this->tileRepository->getStorageDirectory($tile).DIRECTORY_SEPARATOR.$tile->getId().'.'.$tile->getImageExtension();
        $framePath = $this->frameRepository->getFullPath($tile->getFrame());
        $tileThumbnailPath = $this->tileRepository->getThumbnailPath($tile);
        $frameThumbnailPath = $this->frameRepository->getThumbnailPath($tile->getFrame());
        $blendMP4PreviewPath = $this->blendRepository->getMP4PreviewPath($tile->getFrame()->getBlend());
        $blendMP4FinalPath = $this->blendRepository->getMP4FinalPath($tile->getFrame()->getBlend());
        $blendZipPath = $this->blendRepository->getZip($tile->getFrame()->getBlend());

        // update blend storage usage
        $this->blendService->updateSizeTiles($tile->getFrame()->getBlend(), -1.0 * (@filesize($tilePath) + @filesize($tileThumbnailPath)));
        $this->blendService->updateSizeFrames($tile->getFrame()->getBlend(), -1.0 * (@filesize($framePath) + @filesize($frameThumbnailPath)));
        $this->blendService->updateSizeZIP($tile->getFrame()->getBlend(), -1.0 * @filesize($blendZipPath));
        $this->blendService->updateSizeMp4s($tile->getFrame()->getBlend(), -1.0 * (@filesize($blendMP4PreviewPath) + @filesize($blendMP4FinalPath)));

        @unlink($tilePath);
        @unlink($tileThumbnailPath);
        @unlink($framePath);
        @unlink($frameThumbnailPath);
        @unlink($blendMP4PreviewPath);
        @unlink($blendMP4FinalPath);
        @unlink($blendZipPath);

        return new Response('', 200);
    }

    /**
     * Set a owner token for a blend,
     * it be used a security to access assets
     */
    #[Route(path: '/blend/{blend}/token/owner', methods: 'POST')]
    public function setOwnerToken(Request $request, Blend $blend): Response {
        if ($this->isComingFromMaster($request) == false) {
            return new Response('', 403);
        }

        $token = $request->get('token');
        $validity = $request->get('validity');

        if (is_null($token) || is_null($validity)) {
            return new Response('', 400);
        }

        $blend->setOwnerToken($token);

        $date = new DateTime();
        $date->setTimestamp($validity);
        $blend->setOwnerTokenValidity($date);

        $this->entityManager->flush();

        return new Response('', 200);
    }

    /**
     * Set a thumbnail token for a blend,
     * it be used a security to access thumbnails
     */
    #[Route(path: '/blend/{blend}/token/thumbnail', methods: 'POST')]
    public function setThumbnailToken(Request $request, Blend $blend): Response {
        if ($this->isComingFromMaster($request) == false) {
            return new Response('', 403);
        }

        $token = $request->get('token');
        $validity = $request->get('validity');

        if (is_null($token) || is_null($validity)) {
            return new Response('', 400);
        }

        $blend->setThumbnailToken($token);

        $date = new DateTime();
        $date->setTimestamp($validity);
        $blend->setThumbnailTokenValidity($date);

        $this->entityManager->flush();

        return new Response('', 200);
    }

    /**
     * Set generation of MP4s,
     */
    #[Route(path: '/blend/{blend}/generatemp4/{mp4}', methods: 'GET')]
    public function setGenerateMp4(Request $request, Blend $blend, bool $mp4): Response {
        if ($this->isComingFromMaster($request) == false) {
            return new Response('', 403);
        }

        $oldValue = $blend->getGenerateMp4();

        $blend->setGenerateMp4($mp4);

        $this->entityManager->flush();

        if ($oldValue == false && $blend->getGenerateMp4()) {
            if ($this->blendRepository->isFinished($blend)) {
                $taskMP4Final = new Task();
                $taskMP4Final->setType(Task::TYPE_GENERATE_MP4_FINAL);
                $taskMP4Final->setBlend($blend);

                $taskMP4Preview = new Task();
                $taskMP4Preview->setType(Task::TYPE_GENERATE_MP4_PREVIEW);
                $taskMP4Preview->setBlend($blend);

                $this->entityManager->persist($taskMP4Final);
                $this->entityManager->persist($taskMP4Preview);
                $this->entityManager->flush();
            }
        }

        return new Response('', 200);
    }

    /**
     * Ask to generate a partial archive
     */
    #[Route(path: '/blend/{blend}/generatepartialarchive', methods: 'GET')]
    public function generatePartialArchive(Request $request, Blend $blend): Response {
        if ($this->isComingFromMaster($request) == false) {
            return new Response('', 403);
        }

        $taskZip = new Task();
        $taskZip->setType(Task::TYPE_GENERATE_ZIP);
        $taskZip->setBlend($blend);

        $this->entityManager->persist($taskZip);
        $this->entityManager->flush();
        return new Response('', 200);
    }

    /**
     * Set a token for a blend,
     * it be used a security to access data
     */
    #[Route(path: '/monitoring', methods: 'GET')]
    public function getMonitoring(Request $request): JsonResponse {
        if ($this->isComingFromMaster($request) == false) {
            return new JsonResponse($request->getClientIp(), 403);
        }

        $networkUsage = new NetworkUsage();
        $previousNetworkUsage = $networkUsage->get();
        $currentNetworkUsage = $networkUsage->generate();

        $data = [];

        foreach ([new MonitoringHttpd(), new MonitoringCpu(), new MonitoringCpuPsi(), new MonitoringDiskPsi(), new MonitoringRamPsi(), new MonitoringRam(), new MonitoringNetwork('rx', $previousNetworkUsage, $currentNetworkUsage), new MonitoringNetwork('tx', $previousNetworkUsage, $currentNetworkUsage), new MonitoringDisk($this->containerBag, $this->blendRepository), new MonitoringTask($this->entityManager)] as $monitor) {
            $data[$monitor->getType()] = ['value' => $monitor->getValue()];

            if ($monitor->hasMax()) {
                $data[$monitor->getType()]['max'] = $monitor->getMax();
            }
        }

        return new JsonResponse($data, 200);
    }

    /**
     * Get current version
     */
    #[Route(path: '/version', methods: 'GET')]
    public function getVersion(Request $request): Response {
        if ($this->isComingFromMaster($request) == false) {
            return new Response('', 403);
        }

        return new Response(getenv('VERSION'), 200);
    }

    /**
     * Get all tasks
     */
    #[Route(path: '/tasks', methods: 'GET')]
    public function getTasks(Request $request): JsonResponse {
        if ($this->isComingFromMaster($request) == false) {
            return new JsonResponse($request->getClientIp(), 403);
        }

        $data = [];

        foreach ($this->taskRepository->findAll() as $task) {
            $arr = ['id' => $task->getId(), 'status' => $task->getStatus(), 'type' => $task->getType()];

            if (is_object($task->getTile())) {
                $arr['tile'] = $task->getTile()->getId();
            }

            if (is_object($task->getFrame())) {
                $arr['frame'] = $task->getFrame()->getId();
            }

            $arr['blend'] = $task->getBlend()->getId();
            $data [] = $arr;
        }

        return new JsonResponse($data, 200);
    }

    /**
     * Reset a task
     */
    #[Route(path: '/task/{task}/reset', methods: 'GET')]
    public function resetTask(Task $task, Request $request): Response {
        if ($this->isComingFromMaster($request) == false) {
            return new JsonResponse($request->getClientIp(), 403);
        }

        $task->setStatus(Task::STATUS_WAITING);

        $this->entityManager->flush();
        return new Response('', 200);
    }

    /**
     * Remove a task
     */
    #[Route(path: '/task/{task}/delete', methods: 'GET')]
    public function deleteTask(Task $task, Request $request): Response {
        if ($this->isComingFromMaster($request) == false) {
            return new JsonResponse($request->getClientIp(), 403);
        }

        $this->entityManager->remove($task);
        $this->entityManager->flush();

        return new Response('', 200);
    }

    /**
     * I'm alive!
     */
    #[Route(path: '/alive', methods: 'GET')]
    public function iamalive(Request $request): Response {
        if ($this->isComingFromMaster($request) == false) {
            return new Response('', 403);
        }

        return new Response('yes', 200);
    }

    /**
     * Open the route on http so we don't have to ssh the server.
     * Yes it's ugly :(
     */
    #[Route(path: '/database')]
    public function database(): Response {
        $phpBinaryFinder = new PhpExecutableFinder();
        $phpBinaryPath = $phpBinaryFinder->find();

        $cmd = $phpBinaryPath.' '.__DIR__.'/../../bin/console doctrine:schema:update --dump-sql --force  --complete';
        exec($cmd, $output);

        return new Response('<pre>'.join("\n", $output).'</pre>');
    }

    private function isComingFromMaster(Request $request): bool {
        $remote = $request->getClientIp();

        if (is_string($remote) == false) {
            return false;
        }

        $master = $this->containerBag->get('master_url');
        $host_master = parse_url($master, PHP_URL_HOST);

        if (IpUtils::checkIp($host_master, $remote)) { // it is an ip ?
            return true;
        }

        if (IpUtils::checkIp($remote, $this->containerBag->get('master_allowed_networks'))) {
            return true;
        }

        foreach (dns_get_record($host_master, DNS_A + DNS_AAAA) as $record) {
            if (array_key_exists('ip', $record) && IpUtils::checkIp($record['ip'], $remote)) {
                return true;
            }

            if (array_key_exists('ipv6', $record) && IpUtils::checkIp($record['ipv6'], $remote)) {
                return true;
            }
        }

        return false;
    }
}
