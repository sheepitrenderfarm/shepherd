<?php

namespace App\Controller;

use App\Monitoring\MonitoringCpu;
use App\Monitoring\MonitoringCpuPsi;
use App\Monitoring\MonitoringRamPsi;
use App\Monitoring\MonitoringDiskPsi;
use App\Monitoring\MonitoringDisk;
use App\Monitoring\MonitoringHttpd;
use App\Monitoring\MonitoringRam;
use App\Repository\BlendRepository;
use App\Repository\TaskRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller to handle routes for admins
 */
#[Route(path: '/admin')]
class AdminController extends AbstractController {
    public function __construct(
        private ContainerBagInterface $containerBag,
        private BlendRepository $blendRepository,
        private TaskRepository $taskRepository
    ) {
    }

    #[Route(path: '/status')]
    public function status(): Response {
        $monitoring = [];

        foreach ([new MonitoringHttpd(), new MonitoringCpu(), new MonitoringCpuPsi(), new MonitoringDiskPsi(), new MonitoringRamPsi(), new MonitoringRam(), new MonitoringDisk($this->containerBag, $this->blendRepository)] as $monitor) {
            $data = ['type' => $monitor->getType(), 'value' => $monitor->getHumanValue()];

            $monitoring [] = $data;
        }

        return $this->render('status.html.twig', [
            'version' => getenv('VERSION'),
            'tasks' => $this->taskRepository->findAll(),
            'monitoring' => $monitoring,
            'blends' => $this->blendRepository->findAll(),
        ]);
    }
}
