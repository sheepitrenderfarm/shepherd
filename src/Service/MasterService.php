<?php

namespace App\Service;

use App\Entity\Blend;
use App\Entity\Task;
use App\Entity\Tile;
use App\Repository\TileRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpKernel\Log\DebugLoggerConfigurator;
use Symfony\Component\HttpKernel\Log\DebugLoggerInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class MasterService {
    private const string TARGET_FAILED = 'failed.php';
    private const string TARGET_SUCCESS = 'finish.php';

    private string $serverUrl;
    private ?DebugLoggerInterface $monolog;

    public function __construct(
        ContainerBagInterface $containerBag,
        private LoggerInterface $logger,
        private TileRepository $tileRepository
    ) {
        $this->serverUrl = $containerBag->get('master_url').'/api/shepherd/';
        $this->monolog = DebugLoggerConfigurator::getDebugLogger($logger);
    }
    
    public function notifyGeneratedMP4Final(Blend $blend): bool {
        return $this->notify(self::TARGET_SUCCESS, 'mp4_final', $blend);
    }

    public function notifyGeneratedMP4Preview(Blend $blend): bool {
        return $this->notify(self::TARGET_SUCCESS, 'mp4_preview', $blend);
    }

    public function notifyGeneratedZIP(Blend $blend): bool {
        return $this->notify(self::TARGET_SUCCESS, 'zip', $blend);
    }

    public function notifyTaskFailed(Task $task): bool {
        return $this->notify(self::TARGET_FAILED, 'task_'.$task->getType(), $task->getBlend(), $this->getLogStack());
    }

    public function notifyTileFinished(Tile $tile, int $renderTime, int $prepTime, int $memoryUser): bool {
        try {
            $client = HttpClient::create();
            $response = $client->request('POST', $this->serverUrl.self::TARGET_SUCCESS, ['body' => ['type' => 'tile', 'rendertime' => $renderTime, 'preptime' => $prepTime, 'memory_used' => $memoryUser, 'filesize' => filesize($this->tileRepository->getPath($tile)), 'tile' => $tile->getId()]]);

            if ($response->getStatusCode() == 200) {
                return true;
            }
            else {
                $this->logger->error('Failed to notify master of tileFinished, tile:'.$tile->getId().' http code '.$response->getStatusCode());
                return false;
            }
        }
        catch (TransportExceptionInterface $e) {
            $this->logger->error(__method__.' failed to validate tile on master '.$e);
            return false;
        }
    }

    public function notifyTileFailed(Tile $tile): bool {
        try {
            $client = HttpClient::create();
            $response = $client->request('POST', $this->serverUrl.self::TARGET_FAILED, ['body' => ['type' => 'tile', 'id' => $tile->getId()]]);

            if ($response->getStatusCode() == 200) {
                return true;
            }
            else {
                $this->logger->error('Failed to notify master for type: tile tile:'.$tile->getId().' http code '.$response->getStatusCode());
                return false;
            }
        }
        catch (TransportExceptionInterface $e) {
            $this->logger->error('Failed to notify master for type: tile tile:'.$tile->getId().' exception: '.$e->getMessage());
            return false;
        }
    }

    public function notifyTaskReset(Task $task): bool {
        try {
            $client = HttpClient::create();
            $response = $client->request('POST', $this->serverUrl.'reset.php', ['body' => ['type' => $task->getType(), 'id' => $task->getId(), 'blend' => $task->getBlend()]]);
            return $response->getStatusCode() == 200;
        }
        catch (TransportExceptionInterface) {
            return false;
        }
    }

    public function sendMonitoring(string $json): bool {
        $this->logger->error(__method__.' json: '.$json);

        try {
            $client = HttpClient::create();
            $response = $client->request('POST', $this->serverUrl.'monitoring.php', ['body' => $json]);
            return $response->getStatusCode() == 200;
        }
        catch (TransportExceptionInterface) {
            return false;
        }
    }

    /**
     * @param string[] $logs
     *
     */
    private function notify(string $target, string $type, Blend $blend, ?array $logs = null): bool {
        try {
            $client = HttpClient::create();
            $body = ['type' => $type, 'blend' => $blend->getId()];

            if (is_array($logs)) {
                $body['log'] = json_encode($logs);
            }

            $response = $client->request('POST', $this->serverUrl.$target, ['body' => $body]);

            if ($response->getStatusCode() == 200) {
                return true;
            }
            else {
                $this->logger->error('Failed to notify master for type: '.$type.' blend:'.$blend->getId().' http code '.$response->getStatusCode());
                return false;
            }
        }
        catch (TransportExceptionInterface $e) {
            $this->logger->error('Failed to notify master for type: '.$type.' blend:'.$blend->getId().' exception: '.$e->getMessage());
            return false;
        }
    }

    /**
     * @return string[]
     */
    private function getLogStack(): array {
        $app_logs = array_filter($this->monolog->getLogs(),function($log) { return $log['channel'] == 'app';});
        return array_map(function($log) { return $log['timestamp_rfc3339'].' '.$log['priorityName'].' '.$log['message'];}, $app_logs);
    }
}
