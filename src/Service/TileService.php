<?php

namespace App\Service;

use App\Entity\Task;
use App\Entity\Tile;
use App\Image;
use App\Repository\FrameRepository;
use App\Repository\TaskRepository;
use App\Repository\TileRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class TileService extends ThumbnailService {
    public function __construct(
        private ContainerBagInterface $containerBag,
        private EntityManagerInterface $entityManager,
        private TaskRepository $taskRepository,
        private FrameService $frameService,

        TileRepository $tileRepository,
        FrameRepository $frameRepository,
        BlendService $blendService,
        LoggerInterface $logger
    ) {
        parent::__construct(
            $tileRepository,
            $frameRepository,
            $blendService,
            $logger,
        );
    }

    /**
     * Validate a tile
     * Parameters:
     *
     * @return int
     * 0 => No error
     * 302 => File uploaded was not an image.
     * 303 => Failed to upload the image to the server.
     * 308 => Image not at the right dimension
     * 500 => Internal error
     */
    public function validate(Tile $tile, UploadedFile $image): int {
        $a_image = new Image($image->getPathname());

        if ($a_image->isImage() == false) {
            return 302;
        }

        $file_geometry = $a_image->getGeometry();

        if (is_array($file_geometry) == false || array_key_exists('width', $file_geometry) == false || array_key_exists('height', $file_geometry) == false) {
            return 500;
        }

        $frame = $tile->getFrame();

        if ($this->frameService->getWidthPerTile($frame) > 0 && $this->frameService->getHeightPerTile($frame) > 0 && (abs($this->frameService->getWidthPerTile($frame) - $file_geometry['width']) > 2 || abs($this->frameService->getHeightPerTile($frame) - $file_geometry['height']) > 2)) {
            $this->logger->error(__method__.' resolution mismatch file: '.$file_geometry['width'].'x'.$file_geometry['height'].' frame:'.$this->frameService->getWidthPerTile($frame).'x'.$this->frameService->getHeightPerTile($frame));
            return 308;
        }

        $root = $this->tileRepository->getStorageDirectory($tile);

        try {
            $this->logger->debug(__method__.' moving image to '.$root.DIRECTORY_SEPARATOR.$tile->getId().'.'.$tile->getImageExtension());
            @mkdir($root);
            $image->move(
                $root,
                $tile->getId().'.'.$tile->getImageExtension()
            );
        }
        catch (FileException $e) {
            $this->logger->error($e." for validate(".$tile.")");
            return 500;
        }

        $tile->setStatus(Tile::STATUS_FINISHED);

        $this->blendService->updateSizeTiles($tile->getFrame()->getBlend(), filesize($root.DIRECTORY_SEPARATOR.$tile->getId().'.'.$tile->getImageExtension()));

        $this->entityManager->flush();

        if ($this->containerBag->get('tile_thumbnail') == ThumbnailService::GENERATE_ON_VALIDATE) {
            $taskTile = new Task();
            $taskTile->setType(Task::TYPE_GENERATE_TILE_THUMBNAIL);
            $taskTile->setTile($tile);
            $taskTile->setFrame($tile->getFrame());
            $taskTile->setBlend($tile->getFrame()->getBlend());
            $this->entityManager->persist($taskTile);
        }

        // since the tasks are FIFO, we need to cancel the previous tasks to allow the new/next one to be at the end of list
        foreach ($this->taskRepository->findBy(['type' => Task::TYPE_GENERATE_FRAME_THUMBNAIL, 'frame' => $tile->getFrame()->getId(), 'status' => Task::STATUS_WAITING]) as $task) {
            $this->entityManager->remove($task);
        }

        if ($this->containerBag->get('frame_thumbnail') == ThumbnailService::GENERATE_ON_VALIDATE) {
            $task = new Task();
            $task->setType(Task::TYPE_GENERATE_FRAME_THUMBNAIL);
            $task->setFrame($tile->getFrame());
            $task->setBlend($tile->getFrame()->getBlend());
            $this->entityManager->persist($task);
            $this->entityManager->flush();
        }

        if ($this->frameRepository->isFinished($frame)) {
            $this->frameService->onFinish($frame);
        }

        return 0;
    }

    public function generateThumbnail(Tile $tile): bool {
        return $this->generateTileThumbnail($tile);
    }
}
