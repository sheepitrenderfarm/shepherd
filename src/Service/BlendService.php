<?php

namespace App\Service;

use App\Entity\Blend;
use App\Entity\Frame;
use App\Entity\Task;
use App\Entity\Tile;
use App\Image;
use App\Repository\BlendRepository;
use App\Repository\FrameRepository;
use App\Repository\TaskRepository;
use App\Tool\Size;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use PhpZip\Exception\ZipException;
use PhpZip\Exception\InvalidArgumentException;
use PhpZip\ZipFile;
use Psr\Log\LoggerInterface;

class BlendService {
    public function __construct(
        private LoggerInterface $logger,
        private EntityManagerInterface $entityManager,
        private BlendRepository $blendRepository,
        private FrameRepository $frameRepository,
        private TaskRepository $taskRepository) {
    }

    public function onFinish(Blend $blend): void {
        if ($blend->getGenerateMp4()) {
            $taskMP4Final = new Task();
            $taskMP4Final->setType(Task::TYPE_GENERATE_MP4_FINAL);
            $taskMP4Final->setBlend($blend);

            $taskMP4Preview = new Task();
            $taskMP4Preview->setType(Task::TYPE_GENERATE_MP4_PREVIEW);
            $taskMP4Preview->setBlend($blend);

            $this->entityManager->persist($taskMP4Final);
            $this->entityManager->persist($taskMP4Preview);
        }

        $taskZip = new Task();
        $taskZip->setType(Task::TYPE_GENERATE_ZIP);
        $taskZip->setBlend($blend);

        $this->entityManager->persist($taskZip);
        $this->entityManager->flush();
    }

    public function generateMP4Preview(Blend $blend): bool {
        $root = $this->blendRepository->getStorageDirectory($blend);
        $mp4Path = $root.DIRECTORY_SEPARATOR.'output'.DIRECTORY_SEPARATOR.$blend->getId().'_preview.mp4'; // use a better name 444.zip is not humanfriendly

        $mp4TempPath = $mp4Path.'_partial.mp4';

        if (file_exists($mp4Path)) {
            $this->updateSizeMp4s($blend, -1 * filesize($mp4Path));
            @unlink($mp4Path);
        }

        @unlink($mp4TempPath);

        $fps = $blend->getFramerate();

        $framesDirectory = $this->blendRepository->getStorageDirectory($blend).DIRECTORY_SEPARATOR.'frames';
        $firstFrame = $blend->getFrames()->first();
        $picture_extension = $firstFrame->getImageExtension();

        // get geometry
        $firstFramePath = $this->frameRepository->getStorageDirectory($firstFrame).DIRECTORY_SEPARATOR.sprintf("%04d", $firstFrame->getNumber()).'.'.$firstFrame->getImageExtension();
        $anImage = new Image($firstFramePath);
        $geometry = $anImage->getGeometry();

        $maxWidth = 600;

        if ($geometry['width'] > $maxWidth) {
            $height = 4 * (int)(($maxWidth * $geometry['height'] / $geometry['width']) / 4);
            $width = $maxWidth;
            $geometry = ['width' => $width, 'height' => $height];
        }

        $min = PHP_INT_MAX;

        foreach ($blend->getFrames() as $f) {
            $min = min($min, $f->getNumber());
        }

        // don't user 0000 as first image because ffmpeg doesn't like it
        // ffmpeg version 11.7-6:11.7-1~deb8u1, Copyright (c) 2000-2016 the Libav developers
        // built on Jun 12 2016 21:51:35 with gcc 4.9.2 (Debian 4.9.2-10)
        // [image2 demuxer @ 0xc13760] Value 0.000000 for parameter 'start_number' out of range
        // [image2 demuxer @ 0xc13760] Error setting option start_number to value 0000.
        // %4d.png: Numerical result out of range
        if ($min == '0000') {
            $min = '0001';
        }

        // -c:v libx264 — the video codec is libx264 (H.264).
        // -profile:v high — use H.264 High Profile (advanced features, better quality).
        // -crf 20 — constant quality mode, very high quality (lower numbers are higher quality, 18 is the smallest you would want to use).
        // -pix_fmt yuv420p — use YUV pixel format and 4:2:0 Chroma subsampling

        if (chdir($framesDirectory) == false) {
            $this->logger->error(__method__.' failed to chdir, project: '.$blend->getId().' path: '.$framesDirectory);
            return false;
        }

        $cmd = "ffmpeg -r $fps -start_number $min -i \"%4d.$picture_extension\" -s ".$geometry['width']."x".$geometry['height']." -vf \"drawtext=fontfile=Arial.ttf: text='%{frame_num}': start_number=".$min.": x=(w-tw)/2: y=h-(2*lh): fontcolor=black: fontsize=100: box=1: boxcolor=white: boxborderw=10\" -c:v libx264 -crf 18 -pix_fmt yuv420p $mp4TempPath";
        exec($cmd.' 2>&1', $output, $exit_code);
        
        if ($exit_code != 0) {
            $this->logger->debug(__method__.' cmd: '.$cmd);
            $this->logger->error(__method__.' failed for project: '.$blend->getId().' ffmpeg code: '.$exit_code.' output: '.json_encode($output));
            return false;
        }

        $this->updateSizeMp4s($blend, filesize($mp4TempPath));
        $ret = @rename($mp4TempPath, $mp4Path);

        if ($ret == false) {
            $this->logger->error(__method__.' failed for project: '.$blend->getId().' ffmpeg output: '.json_encode($output));
            return false;
        }

        return true;
    }

    public function generateMP4Final(Blend $blend): bool {
        $root = $this->blendRepository->getStorageDirectory($blend);
        $mp4Path = $root.DIRECTORY_SEPARATOR.'output'.DIRECTORY_SEPARATOR.$blend->getId().'_final.mp4'; // use a better name 444.zip is not humanfriendly

        $mp4TempPath = $mp4Path.'_partial.mp4';

        if (file_exists($mp4Path)) {
            $this->updateSizeMp4s($blend, -1 * filesize($mp4Path));
            @unlink($mp4Path);
        }

        if (file_exists($mp4TempPath)) {
            @unlink($mp4TempPath);
        }

        $fps = $blend->getFramerate();

        $framesDirectory = $this->blendRepository->getStorageDirectory($blend).DIRECTORY_SEPARATOR.'frames';
        $firstFrame = $blend->getFrames()->first();
        $picture_extension = $firstFrame->getImageExtension();

        $min = PHP_INT_MAX;

        foreach ($blend->getFrames() as $f) {
            $min = min($min, $f->getNumber());
        }

        if ($min == '0000') {
            $min = '0001';
        }

        chdir($framesDirectory);
        $cmd = "ffmpeg -r $fps -start_number $min -i \"%04d.$picture_extension\" -c:v libx264 -profile:v high -refs 5 -crf 18 -preset slower -bufsize 20M -pix_fmt yuv420p $mp4TempPath";

        exec($cmd.' 2>&1', $output, $exit_code);

        if ($exit_code != 0) {
            $this->logger->debug(__method__.' cmd: '.$cmd);
            $this->logger->error(__method__.' failed for project: '.$blend->getId().' ffmpeg code: '.$exit_code.' output: '.json_encode($output));
            return false;
        }

        $this->updateSizeMp4s($blend, filesize($mp4TempPath));
        @rename($mp4TempPath, $mp4Path);
        $ret = file_exists($mp4Path);

        if ($ret == false) {
            $this->logger->error(__method__.' failed for project: '.$blend->getId().' ffmpeg output: '.json_encode($output));
        }

        return $ret;
    }

    public function generateZIP(Blend $blend): bool {
        $root = $this->blendRepository->getStorageDirectory($blend);
        $framesDirectory = $root.DIRECTORY_SEPARATOR.'frames';
        $zipPath = $this->blendRepository->getZip($blend); // use a better name 444.zip is not humanfriendly

        if (file_exists($zipPath)) {
            $this->updateSizeZIP($blend, -1 * filesize($zipPath));
            @unlink($zipPath);
        }

        $zipFile = new ZipFile();

        try {
            foreach (glob($framesDirectory.DIRECTORY_SEPARATOR.'*') as $path) {
                $zipFile->addFile($path); // add an entry from the file
            }

            $zipFile->saveAsFile($zipPath);
            $zipFile->close(); // close archive
        }
        catch (ZipException $e) {
            $this->logger->error(__method__.' failed to generate zip '.$zipPath.' exception: '.$e->getMessage());
            return false;
        }
        catch (InvalidArgumentException) { // if the target zipPath parent directory doesn't exist.
        }
        finally {
            $zipFile->close();
        }

        $this->updateSizeZIP($blend, filesize($zipPath));
        return file_exists($zipPath);
    }

    /**
     * @return Tile[]
     */
    public function addBlend(int $id, float $framerate, int $width, int $height, bool $mp4, array $frames, ?string $tokenOwner, ?DateTime $tokenOwnerValidity, ?string $tokenThumbnail, ?DateTime $tokenThumbnailValidity): array {
        $tiles = [];

        $blend = new Blend();
        $blend->setId($id);
        $blend->setFramerate($framerate);
        $blend->setWidth($width);
        $blend->setHeight($height);
        $blend->setGenerateMp4($mp4);

        if ($tokenOwner != null && $tokenOwnerValidity != null) {
            $blend->setOwnerToken($tokenOwner);
            $blend->setOwnerTokenValidity($tokenOwnerValidity);
        }

        if ($tokenThumbnail != null && $tokenThumbnailValidity != null) {
            $blend->setThumbnailToken($tokenThumbnail);
            $blend->setThumbnailTokenValidity($tokenThumbnailValidity);
        }

        $this->entityManager->persist($blend);

        foreach ($frames as $frameJson) {
            $frame = new Frame();
            $frame->setId($frameJson['uid']);
            $frame->setType($frameJson['type']);
            $frame->setNumber((int)$frameJson['number']);
            $frame->setImageExtension($frameJson['image_extension']);
            $blend->addFrame($frame);
            $this->entityManager->persist($frame);

            if (array_key_exists('tiles', $frameJson) == false) {
                if ($frameJson['type'] == 'full') {
                    $tile = new Tile();
                    $tile->setNumber(0);
                    $tile->setId($frameJson['uid']);
                    $tile->setStatus(Tile::STATUS_PROCESSING);
                    $tile->setToken($frameJson['token']);
                    $frame->addTile($tile);

                    $this->entityManager->persist($tile);
                    $tiles [] = $tile;
                }
            }
            else {
                foreach ($frameJson['tiles'] as $tileJson) {
                    $tile = new Tile();
                    $tile->setNumber((int)$tileJson['number']);
                    $tile->setId($tileJson['uid']);
                    $tile->setStatus(Tile::STATUS_PROCESSING);
                    $tile->setToken($tileJson['token']);
                    $frame->addTile($tile);

                    $this->entityManager->persist($tile);
                    $tiles [] = $tile;
                }
            }
        }

        $this->entityManager->flush();

        $this->createDirectories($blend);

        return $tiles;
    }

    public function createDirectories(Blend $blend): void {
        $root = $this->blendRepository->getStorageDirectory($blend);
        @mkdir($root);
        @mkdir($root.DIRECTORY_SEPARATOR.'output');
        @mkdir($root.DIRECTORY_SEPARATOR.'frames');
        @mkdir($root.DIRECTORY_SEPARATOR.'thumbnails');
        @mkdir($root.DIRECTORY_SEPARATOR.'thumbnails'.DIRECTORY_SEPARATOR.'frames');
        @mkdir($root.DIRECTORY_SEPARATOR.'thumbnails'.DIRECTORY_SEPARATOR.'tiles');
        @mkdir($root.DIRECTORY_SEPARATOR.'tiles');
        @mkdir($root.DIRECTORY_SEPARATOR.'tiles'.DIRECTORY_SEPARATOR.'frames');
    }

    public function delBlend(Blend $blend): bool {
        $root = $this->blendRepository->getStorageDirectory($blend);
        $this->rrmdir($root);

        foreach ($blend->getFrames() as $frame) {
            foreach ($frame->getTiles() as $tile) {
                $this->entityManager->remove($tile);
            }

            $this->entityManager->remove($frame);
        }

        foreach ($this->taskRepository->findBy(['blend' => $blend->getId()]) as $task) {
            $this->entityManager->remove($task);
        }

        $this->entityManager->remove($blend);
        $this->entityManager->flush();

        return true;
    }

    /**
     * Calculate storage size used by a blend
     *
     */
    public function setSize(Blend $blend): void {
        $root = $this->blendRepository->getStorageDirectory($blend);

        $tilesDirectory = $root.DIRECTORY_SEPARATOR.'tiles';
        $tilesThumbnailsDirectory = $root.DIRECTORY_SEPARATOR.'thumbnails'.DIRECTORY_SEPARATOR.'tiles';
        $framesThumbnailsDirectory = $root.DIRECTORY_SEPARATOR.'thumbnails'.DIRECTORY_SEPARATOR.'frames';
        $framesDirectory = $root.DIRECTORY_SEPARATOR.'frames';
        $MP4PreviewPath = $this->blendRepository->getMP4PreviewPath($blend);
        $MP4FinalPath = $this->blendRepository->getMP4FinalPath($blend);
        $ZipPath = $this->blendRepository->getZip($blend);

        $blend->setSizeTiles(Size::dirSize($tilesDirectory) + Size::dirSize($tilesThumbnailsDirectory));
        $blend->setSizeFrames(Size::dirSize($framesDirectory) + Size::dirSize($framesThumbnailsDirectory));
        $blend->setSizeZIP(@filesize($ZipPath));
        $blend->setSizeMP4s(@filesize($MP4PreviewPath) + @filesize($MP4FinalPath));

        $this->blendRepository->updatePredictedTotalSize($blend);
        $this->entityManager->flush();
     }

    /**
     * Update storage used by a blend
     *
     * @param int $update add/remove to the current storage usage
     */
    public function updateSizeTiles(Blend $blend, int $update): void {
        if ($update != 0) {
            $blend->setSizeTiles($blend->getSizeTiles() + $update);
            $this->blendRepository->updatePredictedTotalSize($blend);
            $this->entityManager->flush();
        }
    }

    /**
     * Update storage used by a blend
     *
     * @param int $update add/remove to the current storage usage
     */
    public function updateSizeFrames(Blend $blend, int $update): void {
        if ($update != 0) {
            $blend->setSizeFrames($blend->getSizeFrames() + $update);
            $this->blendRepository->updatePredictedTotalSize($blend);
            $this->entityManager->flush();
        }
    }

    /**
     * Update storage used by a blend
     *
     * @param int $update add/remove to the current storage usage
     */
    public function updateSizeZIP(Blend $blend, int $update): void {
        if ($update != 0) {
            $blend->setSizeZIP($blend->getSizeZIP() + $update);
            $this->blendRepository->updatePredictedTotalSize($blend);
            $this->entityManager->flush();
        }
    }

    /**
     * Update storage used by a blend
     *
     * @param int $update add/remove to the current storage usage
     */
    public function updateSizeMp4s(Blend $blend, int $update): void {
        if ($update != 0) {
            $blend->setSizeMP4s($blend->getSizeMP4s() + $update);
            $this->blendRepository->updatePredictedTotalSize($blend);
            $this->entityManager->flush();
        }
    }

    private function rrmdir(string $src): void {
        if (file_exists($src)) {
            $dir = opendir($src);

            while (false !== ($file = readdir($dir))) {
                if (($file != '.') && ($file != '..')) {
                    $full = $src.'/'.$file;

                    if (is_dir($full)) {
                        $this->rrmdir($full);
                    }
                    else {
                        unlink($full);
                    }
                }
            }

            closedir($dir);
            rmdir($src);
        }
    }

    public function isOwnerTokenValid(Blend $blend, string $token): bool {
        return $blend->getOwnerToken() == $token && new \DateTime() < $blend->getOwnerTokenValidity();
    }

    public function isThumbnailTokenValid(Blend $blend, string $token): bool {
        return $blend->getThumbnailToken() == $token && new \DateTime() < $blend->getThumbnailTokenValidity();
    }
}
