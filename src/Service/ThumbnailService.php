<?php

namespace App\Service;

use App\Entity\Tile;
use App\Image;
use App\Repository\FrameRepository;
use App\Repository\TileRepository;
use Psr\Log\LoggerInterface;

class ThumbnailService {
    public const GENERATE_ON_DEMAND = 'ON_DEMAND';
    public const GENERATE_ON_VALIDATE = 'ON_VALIDATE';

    public function __construct(
        protected TileRepository $tileRepository,
        protected FrameRepository $frameRepository,
        protected BlendService $blendService,
        protected LoggerInterface $logger
    ) {
    }

    public function generateTileThumbnail(Tile $tile): bool {
        $this->logger->debug(__method__.' '.$tile);

        $sourcePath = $this->tileRepository->getPath($tile);
        $targetPath = $this->tileRepository->getThumbnailPath($tile);

        $this->logger->debug(__method__.' '.$sourcePath.' => '.$targetPath);

        if (file_exists($targetPath)) {
            // thumbnail already done :)
            return true;
        }

        if ($tile->getFrame()->getType() == 'full') {
            // on "full" frame, the tile and frame image are actually the same.
            // let's check if the image exists, if so no need for a transform
            $thumbnailFramePath = $this->frameRepository->getThumbnailPath($tile->getFrame());

            if (file_exists($thumbnailFramePath)) {
                // no thumbnail generation needed, we already have the file
                copy($thumbnailFramePath, $targetPath);
                $this->blendService->updateSizeTiles($tile->getFrame()->getBlend(), filesize($targetPath));
                return true;
            }
        }

        $img = new Image($sourcePath);
        $ret = $img->generateThumbnail(200, 0, $targetPath);

        if ($ret) {
            $this->blendService->updateSizeTiles($tile->getFrame()->getBlend(), filesize($targetPath));
        }

        return $ret;
    }
}