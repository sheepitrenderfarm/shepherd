<?php

namespace App\Service;

use App\Entity\Task;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class TaskService {
    public function __construct(private EntityManagerInterface $entityManager, private MasterService $masterService, private BlendService $blendService, private FrameService $frameService, private TileService $tileService, private LoggerInterface $logger) {
    }

    public function execute(Task $task): bool {
        $this->logger->debug(__method__.' type: '.$task->getType());
        $ret = true;

        if ($task->getType() == Task::TYPE_GENERATE_FRAME_THUMBNAIL && is_object($task->getFrame())) {
            $this->logger->debug(__method__.' will : frameService->generateImageThumbnail');
            $ret = $this->frameService->generateImageThumbnail($task->getFrame());
        }
        elseif ($task->getType() == Task::TYPE_GENERATE_TILE_THUMBNAIL && is_object($task->getTile())) {
            $this->logger->debug(__method__.' will : tileService->generateThumbnail');
            $ret = $this->tileService->generateThumbnail($task->getTile());
        }
        elseif ($task->getType() == Task::TYPE_GENERATE_FRAME && is_object($task->getFrame())) {
            $this->logger->debug(__method__.' will : frameService->generateImage');
            $ret = $this->frameService->generateImage($task->getFrame());
        }
        elseif ($task->getType() == Task::TYPE_GENERATE_MP4_FINAL) {
            $this->logger->debug(__method__.' will : blendService->generateMP4Final');

            if ($this->blendService->generateMP4Final($task->getBlend())) {
                if ($this->masterService->notifyGeneratedMP4Final($task->getBlend()) == false) {
                    // most like the master is down, let's retry later
                    $task = new Task();
                    $task->setType(Task::TYPE_NOTIFY_MP4_FINAL);
                    $task->setBlend($task->getBlend());
                    $this->entityManager->persist($task);
                    $this->entityManager->flush();
                    return false;
                }
                else {
                    return true;
                }
            }
            else {
                $ret = false;
            }
        }
        elseif ($task->getType() == Task::TYPE_GENERATE_MP4_PREVIEW) {
            $this->logger->debug(__method__.' will : blendService->generateMP4Preview');

            if ($this->blendService->generateMP4Preview($task->getBlend())) {
                if ($this->masterService->notifyGeneratedMP4Preview($task->getBlend()) == false) {
                    // most like the master is down, let's retry later
                    $task = new Task();
                    $task->setType(Task::TYPE_NOTIFY_MP4_PREVIEW);
                    $task->setBlend($task->getBlend());
                    $this->entityManager->persist($task);
                    $this->entityManager->flush();
                    return false;
                }
                else {
                    return true;
                }
            }
            else {
                $ret = false;
            }
        }
        elseif ($task->getType() == Task::TYPE_GENERATE_ZIP) {
            $this->logger->debug(__method__.' will : blendService->generateZIP');

            if ($this->blendService->generateZIP($task->getBlend())) {
                if ($this->masterService->notifyGeneratedZIP($task->getBlend()) == false) {
                    // most like the master is down, let's retry later
                    $task = new Task();
                    $task->setType(Task::TYPE_NOTIFY_ZIP);
                    $task->setBlend($task->getBlend());
                    $this->entityManager->persist($task);
                    $this->entityManager->flush();
                    $ret = false;
                }
                else {
                    return true;
                }
            }
        }
        elseif ($task->getType() == Task::TYPE_DELETE_BLEND) {
            $this->logger->debug(__method__.' will : blendService->delBlend');
            $ret = $this->blendService->delBlend($task->getBlend());
        }
        elseif ($task->getType() == Task::TYPE_VALIDATE_TILE && is_object($task->getTile())) {
            $renderTime = intval($task->getData()['rendertime']);
            $memoryUsed = intval($task->getData()['memory']);
            return $this->masterService->notifyTileFinished($task->getTile(), $renderTime, 0, $memoryUsed);
        }
        elseif ($task->getType() == Task::TYPE_NOTIFY_ZIP) {
            return $this->masterService->notifyGeneratedZIP($task->getBlend());
        }
        elseif ($task->getType() == Task::TYPE_NOTIFY_MP4_FINAL) {
            return $this->masterService->notifyGeneratedMP4Final($task->getBlend());
        }
        elseif ($task->getType() == Task::TYPE_NOTIFY_MP4_PREVIEW) {
            return $this->masterService->notifyGeneratedMP4Preview($task->getBlend());
        }

        if ($ret == false && $task->isNotify() == false) {
            $this->masterService->notifyTaskFailed($task);
            return false;
        }

        return $ret;
    }

    /**
     * Reset a task to be used later
     */
    public function reset(Task $task): bool {
        $task->setStartTime(null);
        $task->setStatus(Task::STATUS_WAITING);

        return true;
    }
}
