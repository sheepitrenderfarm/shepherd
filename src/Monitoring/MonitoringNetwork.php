<?php

namespace App\Monitoring;

use App\Tool\Size;

class MonitoringNetwork extends MonitoringComponentAbstract {
    /**
     * @param array{rx: float, tx: float, time: int} $previous
     * @param array{rx: float, tx: float, time: int} $current
     */
    public function __construct(private string $type, private array $previous, private array $current) {
    }

    public function getType(): string {
        return 'network-'.$this->type;
    }

    public function getValue(): float {
        if ($this->previous['time'] != $this->current['time']) {
            return (float)($this->current[$this->type] - $this->previous[$this->type]) / (float)($this->current['time'] - $this->previous['time']);
        }
        else {
            return 0.0;
        }
    }

    public function getHumanValue(): string {
        return Size::humanSize($this->getValue(), 'bps');
    }
}
