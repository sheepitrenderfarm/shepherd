<?php

namespace App\Monitoring;

use App\Entity\Task;
use Doctrine\ORM\EntityManagerInterface;

class MonitoringTask extends MonitoringComponentAbstract {
    public function __construct(private EntityManagerInterface $entityManager) {
    }

    public function getType(): string {
        return 'task';
    }

    public function getValue(): float {
        $taskEntity = $this->entityManager->getRepository(Task::class);
        return count($taskEntity->findAll());
    }

    public function getHumanValue(): string {
        return strval($this->getValue());
    }
}
