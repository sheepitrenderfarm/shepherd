<?php

namespace App\Monitoring;

/**
 * Definition of a monitoring component
 */
abstract class MonitoringComponentAbstract {
    abstract public function getType(): string;

    abstract public function getValue(): float;

    abstract public function getHumanValue(): string;

    public function hasMax(): bool {
        return false;
    }

    public function getMax(): float {
        return 0.0; // should never be call if hasMax is false
    }
}
