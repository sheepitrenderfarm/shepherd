#!/usr/bin/env python3
#
# Copyright (C) 2015 Laurent CLOUET
# Author Laurent CLOUET <laurent.clouet@nopnop.net>
#
from OpenImageIO import ImageBuf, ImageBufAlgo, ROI
import sys
import re
import os

def main(argv):
	if len(argv) != 5:
		raise SystemExit("Usage: %s src dest width height" % argv[0])

	def choose_channels(image, regex):
		indices = []
		for index, name in enumerate(image.spec().channelnames):
			match = re.fullmatch(regex, name)
			if match and match.group(1) in ("R", "G", "B", "A"):
				indices.append(index)
		return indices

	hdr = ImageBuf(argv[1])

	# Try to find channels named `Composite.Combined.`. Failing that, any `.Combined.` that starts with `View`. Failing THAT, any `.Combined.` - finally, for single-layer EXR, just the channel names without prefix ("R", "G", "B")
	channels = choose_channels(hdr, r"^Composite.Combined\.(.)$")
	if not channels:
		channels = choose_channels(hdr, r"^View.*\.Combined\.(.)$")
	if not channels:
		channels = choose_channels(hdr, r"^.*\.Combined\.(.)$")
	if not channels:
		channels = choose_channels(hdr, r"^(.*)$")
	if not channels:
		raise Exception(f"No Channels with proper names found, available channels:\n{hdr.spec().channelnames} on file: {argv[1]}")

	# Remove any extraneous channels. OIIO automatically keeps them in RGBA order, so we just need to specify which ones we want
	stripped = ImageBufAlgo.channels(hdr, tuple(channels), nthreads = 1)

	# Color space conversion
	ldr = ImageBufAlgo.colorconvert(stripped, "linear", "sRGB", nthreads = 1)

	# resize before saving
	width = int(argv[3])
	height = int(argv[4])
	resized_image = ImageBufAlgo.resize(ldr, roi = ROI(0, width, 0, height, 0, 1, 0, ldr.nchannels), nthreads = 1)

	# Save
	resized_image.write(argv[2])

if __name__ == "__main__":
	main(sys.argv)
