<?php

namespace App\Tests;

use App\Service\BlendService;

class ToolsService {
    private int $autoincrement_tile = 1;
    private int $autoincrement_frame = 1;

    public function __construct(private BlendService $blendService) {
    }

    public function addBlend(string $type, int $framesCount, int $tilesCount, int $width, int $height): int {
        $id = rand(1000, 20000);

        $frames = [];

        for ($f = 0; $f < $framesCount; $f++) {
            $tiles = [];

            for ($t = 0; $t < $tilesCount; $t++) {
                $tiles [] = ['uid' => $this->autoincrement_tile++, 'number' => $t, 'image_extension' => 'png', 'token' => uniqid()];
            }

            $frames [] = ['type' => $type, 'uid' => $this->autoincrement_frame++, 'number' => $f, 'image_extension' => 'png', 'tiles' => $tiles];
        }

        $this->blendService->addBlend($id, 25, $width, $height, true, $frames, null, null, null, null);

        return $id;
    }
}
