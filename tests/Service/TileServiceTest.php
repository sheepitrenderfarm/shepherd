<?php

namespace App\Tests\Service;

use App\Repository\BlendRepository;
use App\Repository\TaskRepository;
use App\Repository\TileRepository;
use App\Service\TileService;
use App\Tests\Tools;
use App\Tests\ToolsService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class TileServiceTest extends KernelTestCase {
    private BlendRepository $blendRepository;
    private TaskRepository $taskRepository;
    private ToolsService $toolsService;
    private TileService $tileService;
    private TileRepository $tileRepository;
    private Tools $tools;

    protected function setUp(): void {
        $this->blendRepository = self::getContainer()->get(BlendRepository::class);
        $this->taskRepository = self::getContainer()->get(TaskRepository::class);
        $this->tileRepository = self::getContainer()->get(TileRepository::class);
        $this->tileService = self::getContainer()->get(TileService::class);
        $this->toolsService = self::getContainer()->get(ToolsService::class);
        $this->tools = self::getContainer()->get(Tools::class);
    }

    public function testOnlyOneThumbnailTaskPerFrame(): void {
        $blendId = $this->toolsService->addBlend("layer", 1, 10, 256, 256);

        $blend = $this->blendRepository->find($blendId);
        $this->assertNotNull($blend);

        $frame = $blend->getFrames()->first();

        $this->assertEquals(0, $this->tileService->validate($frame->getTiles()->get(0), new UploadedFile($this->tools->generateFakeImage(256, 256), '0.png', 'image/png', null, true)));

        // one task for frame thumbnail is created
        // one task for the tile thumbnail is created
        $this->assertEquals(2, $this->taskRepository->count(['frame' => $frame->getId()]));

        // validate a new tile, the old task must be deleted and a new one will be created
        $this->assertEquals(0, $this->tileService->validate($frame->getTiles()->get(1), new UploadedFile($this->tools->generateFakeImage(256, 256), '0.png', 'image/png', null, true)));
        $this->assertEquals(3, $this->taskRepository->count(['frame' => $frame->getId()]));
    }

    public function testGenerateThumbnail(): void {
        $blendId = $this->toolsService->addBlend("full", 10, 1, 1920, 1080);

        $blend = $this->blendRepository->find($blendId);
        $this->assertNotNull($blend);

        // validate ONE frame
        $firstFrame = $blend->getFrames()->first();
        $firstTile = $firstFrame->getTiles()->first();
        $this->tools->validateTile($firstTile, new UploadedFile($this->tools->generateFakeImage(256, 256), '0.png', 'image/png', null, true));

        $this->assertTrue($this->tileService->generateThumbnail($firstTile));
        $this->assertTrue(file_exists($this->tileRepository->getThumbnailPath($firstTile)));
    }
}
