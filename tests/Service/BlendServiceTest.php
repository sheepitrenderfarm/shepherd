<?php

namespace App\Tests\Service;

use App\Entity\Blend;
use App\Repository\BlendRepository;
use App\Service\BlendService;
use App\Service\FrameService;
use App\Tests\Tools;
use App\Tests\ToolsService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class BlendServiceTest extends KernelTestCase {
    private BlendService $blendService;
    private BlendRepository $blendRepository;

    private Blend $blend;

    protected function setUp(): void {
        $this->blendRepository = self::getContainer()->get(BlendRepository::class);
        $this->blendService = self::getContainer()->get(BlendService::class);
        $frameService = self::getContainer()->get(FrameService::class);
        $toolsService = self::getContainer()->get(ToolsService::class);
        $tools = self::getContainer()->get(Tools::class);

        $blendId = $toolsService->addBlend("full", 10, 1, 1920, 1080);

        $this->blend = $this->blendRepository->find($blendId);
        $this->assertNotNull($this->blend);

        foreach ($this->blend->getFrames() as $frame) {
            $tools->validateTile($frame->getTiles()->get(0), new UploadedFile($tools->generateFakeImage(256, 256), '0.png', 'image/png', null, true));

            $this->assertTrue($frameService->generateImage($frame));
        }
    }

    public function testMP4PreviewGeneration(): void {
        $this->assertTrue($this->blendService->generateMP4Preview($this->blend));
        $this->assertFileExists($this->blendRepository->getStorageDirectory($this->blend).DIRECTORY_SEPARATOR.'output'.DIRECTORY_SEPARATOR.$this->blend->getId().'_preview.'.'mp4');
    }

    public function testMP4FinalGeneration(): void {
        $this->assertTrue($this->blendService->generateMP4Final($this->blend));
        $this->assertFileExists($this->blendRepository->getStorageDirectory($this->blend).DIRECTORY_SEPARATOR.'output'.DIRECTORY_SEPARATOR.$this->blend->getId().'_final.'.'mp4');
    }

    public function testZIPGeneration(): void {
        $this->assertTrue($this->blendService->generateZIP($this->blend));
        $this->assertFileExists($this->blendRepository->getStorageDirectory($this->blend).DIRECTORY_SEPARATOR.'output'.DIRECTORY_SEPARATOR.$this->blend->getId().'.'.'zip');
    }
}
