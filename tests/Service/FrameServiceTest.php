<?php

namespace App\Tests\Service;

use App\Repository\BlendRepository;
use App\Repository\FrameRepository;
use App\Repository\TileRepository;
use App\Service\FrameService;
use App\Service\TileService;
use App\Tests\Tools;
use App\Tests\ToolsService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FrameServiceTest extends KernelTestCase {
    private TileService $tileService;
    private FrameService $frameService;
    private BlendRepository $blendRepository;
    private FrameRepository $frameRepository;
    private TileRepository $tileRepository;
    private ToolsService $toolsService;
    private Tools $tools;

    protected function setUp(): void {
        $this->blendRepository = self::getContainer()->get(BlendRepository::class);
        $this->frameRepository = self::getContainer()->get(FrameRepository::class);
        $this->tileRepository = self::getContainer()->get(TileRepository::class);
        $this->frameService = self::getContainer()->get(FrameService::class);
        $this->tileService = self::getContainer()->get(TileService::class);
        $this->toolsService = self::getContainer()->get(ToolsService::class);
        $this->tools = self::getContainer()->get(Tools::class);
    }

    public function testOnFinishTileForFrameFull(): void {
        $blendId = $this->toolsService->addBlend("full", 10, 1, 1920, 1080);

        $blend = $this->blendRepository->find($blendId);
        $this->assertNotNull($blend);

        $frame = $blend->getFrames()->get(0);

        $this->tools->validateTile($frame->getTiles()->get(0), new UploadedFile($this->tools->generateFakeImage(256, 256), '0.png', 'image/png', null, true));

        $this->assertTrue($this->frameService->generateImage($frame));
        $this->assertFileExists($this->frameRepository->getStorageDirectory($frame).DIRECTORY_SEPARATOR.sprintf("%04d", $frame->getNumber()).'.'.$frame->getImageExtension());
    }

    public function testOnFinishTileForFrameRegion(): void {

        $blendId = $this->toolsService->addBlend("region", 1, 4, 1920, 1080);

        $blend = $this->blendRepository->find($blendId);
        $this->assertNotNull($blend);

        $frame = $blend->getFrames()->get(0);
        $this->tools->validateTile($frame->getTiles()->get(0), new UploadedFile($this->tools->generateFakeImage(256, 256), '0.png', 'image/png', null, true));
        $this->tools->validateTile($frame->getTiles()->get(1), new UploadedFile($this->tools->generateFakeImage(256, 256), '0.png', 'image/png', null, true));
        $this->tools->validateTile($frame->getTiles()->get(2), new UploadedFile($this->tools->generateFakeImage(256, 256), '0.png', 'image/png', null, true));
        $this->tools->validateTile($frame->getTiles()->get(3), new UploadedFile($this->tools->generateFakeImage(256, 256), '0.png', 'image/png', null, true));

        $this->assertTrue($this->frameService->generateImage($frame));
        $this->assertFileExists($this->frameRepository->getStorageDirectory($frame).DIRECTORY_SEPARATOR.sprintf("%04d", $frame->getNumber()).'.'.$frame->getImageExtension());
    }

    public function testOnFinishTileForFrameLayer(): void {

        $blendId = $this->toolsService->addBlend("layer", 1, 10, 1920, 1080);

        $blend = $this->blendRepository->find($blendId);
        $this->assertNotNull($blend);

        $frame = $blend->getFrames()->get(0);
        $this->tools->validateTile($frame->getTiles()->get(0), new UploadedFile($this->tools->generateFakeImage(256, 256), '0.png', 'image/png', null, true));
        $this->tools->validateTile($frame->getTiles()->get(1), new UploadedFile($this->tools->generateFakeImage(256, 256), '0.png', 'image/png', null, true));
        $this->tools->validateTile($frame->getTiles()->get(2), new UploadedFile($this->tools->generateFakeImage(256, 256), '0.png', 'image/png', null, true));
        $this->tools->validateTile($frame->getTiles()->get(3), new UploadedFile($this->tools->generateFakeImage(256, 256), '0.png', 'image/png', null, true));
        $this->tools->validateTile($frame->getTiles()->get(4), new UploadedFile($this->tools->generateFakeImage(256, 256), '0.png', 'image/png', null, true));
        $this->tools->validateTile($frame->getTiles()->get(5), new UploadedFile($this->tools->generateFakeImage(256, 256), '0.png', 'image/png', null, true));
        $this->tools->validateTile($frame->getTiles()->get(6), new UploadedFile($this->tools->generateFakeImage(256, 256), '0.png', 'image/png', null, true));
        $this->tools->validateTile($frame->getTiles()->get(7), new UploadedFile($this->tools->generateFakeImage(256, 256), '0.png', 'image/png', null, true));
        $this->tools->validateTile($frame->getTiles()->get(8), new UploadedFile($this->tools->generateFakeImage(256, 256), '0.png', 'image/png', null, true));
        $this->tools->validateTile($frame->getTiles()->get(9), new UploadedFile($this->tools->generateFakeImage(256, 256), '0.png', 'image/png', null, true));

        $this->assertTrue($this->frameService->generateImage($frame));
        $this->assertFileExists($this->frameRepository->getStorageDirectory($frame).DIRECTORY_SEPARATOR.sprintf("%04d", $frame->getNumber()).'.'.$frame->getImageExtension());
    }

    public function testResolutionPerTileForFrameFull10(): void {
        $blendId = $this->toolsService->addBlend("full", 10, 10, 1920, 1080);

        $blend = $this->blendRepository->find($blendId);
        $this->assertNotNull($blend);

        $frame = $blend->getFrames()->get(0);

        $this->assertEquals(1920, $this->frameService->getWidthPerTile($frame));
        $this->assertEquals(1080, $this->frameService->getHeightPerTile($frame));
    }

    public function testResolutionPerTileForFrameFull20(): void {
        $blendId = $this->toolsService->addBlend("full", 10, 20, 1920, 1080);

        $blend = $this->blendRepository->find($blendId);
        $this->assertNotNull($blend);

        $frame = $blend->getFrames()->get(0);

        $this->assertEquals(1920, $this->frameService->getWidthPerTile($frame));
        $this->assertEquals(1080, $this->frameService->getHeightPerTile($frame));
    }

    public function testResolutionPerTileForFrameRegion4(): void {
        $blendId = $this->toolsService->addBlend("region", 1, 4, 1920, 1080);

        $blend = $this->blendRepository->find($blendId);
        $this->assertNotNull($blend);

        $frame = $blend->getFrames()->get(0);

        $this->assertEquals(1920 / 2, $this->frameService->getWidthPerTile($frame));
        $this->assertEquals(1080 / 2, $this->frameService->getHeightPerTile($frame));
    }

    public function testResolutionPerTileForFrameRegion16(): void {
        $blendId = $this->toolsService->addBlend("region", 1, 16, 1920, 1080);

        $blend = $this->blendRepository->find($blendId);
        $this->assertNotNull($blend);

        $frame = $blend->getFrames()->get(0);

        $this->assertEquals(1920 / 4, $this->frameService->getWidthPerTile($frame));
        $this->assertEquals(1080 / 4, $this->frameService->getHeightPerTile($frame));
    }

    public function testResolutionPerTileForFrameLayer10(): void {
        $blendId = $this->toolsService->addBlend("layer", 1, 10, 1920, 1080);

        $blend = $this->blendRepository->find($blendId);
        $this->assertNotNull($blend);

        $frame = $blend->getFrames()->get(0);

        $this->assertEquals(1920, $this->frameService->getWidthPerTile($frame));
        $this->assertEquals(1080, $this->frameService->getHeightPerTile($frame));
    }

    public function testResolutionPerTileForFrameLayer20(): void {
        $blendId = $this->toolsService->addBlend("layer", 1, 20, 1920, 1080);

        $blend = $this->blendRepository->find($blendId);
        $this->assertNotNull($blend);

        $frame = $blend->getFrames()->get(0);

        $this->assertEquals(1920, $this->frameService->getWidthPerTile($frame));
        $this->assertEquals(1080, $this->frameService->getHeightPerTile($frame));
    }

    public function testGenerateThumbnailOrderTask1(): void {
        // run the tile thumbnail AFTER the frame thumbnail task
        $blendId = $this->toolsService->addBlend("full", 10, 1, 1920, 1080);

        $blend = $this->blendRepository->find($blendId);
        $this->assertNotNull($blend);

        // validate ONE frame
        $firstFrame = $blend->getFrames()->first();
        $firstTile = $firstFrame->getTiles()->first();
        $this->tools->validateTile($firstTile, new UploadedFile($this->tools->generateFakeImage(256, 256), '0.png', 'image/png', null, true));

        // run frame task, the tile task was never run
        $this->assertTrue($this->frameService->generateImageThumbnail($firstFrame));
        $this->assertTrue(file_exists($this->frameRepository->getThumbnailPath($firstFrame)));


        $this->assertTrue($this->tileService->generateThumbnail($firstTile));
        $this->assertTrue(file_exists($this->tileRepository->getThumbnailPath($firstTile)));
    }

    public function testGenerateThumbnailOrderTask2(): void {
        // run the tile thumbnail BEFORE the frame thumbnail task

        $blendId = $this->toolsService->addBlend("full", 10, 1, 1920, 1080);

        $blend = $this->blendRepository->find($blendId);
        $this->assertNotNull($blend);

        // validate ONE frame
        $firstFrame = $blend->getFrames()->first();
        $firstTile = $firstFrame->getTiles()->first();
        $this->tools->validateTile($firstTile, new UploadedFile($this->tools->generateFakeImage(256, 256), '0.png', 'image/png', null, true));

        // run tile task, the frame task was never run
        $this->assertTrue($this->tileService->generateThumbnail($firstTile));
        $this->assertTrue(file_exists($this->tileRepository->getThumbnailPath($firstTile)));

        $this->assertTrue($this->frameService->generateImageThumbnail($firstFrame));
        $this->assertTrue(file_exists($this->frameRepository->getThumbnailPath($firstFrame)));
    }
}
