<?php

namespace App\Tests\Service;

use App\Entity\Task;
use App\Repository\BlendRepository;
use App\Service\TaskService;
use App\Tests\ToolsService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class TaskServiceTest extends KernelTestCase {
    private EntityManagerInterface $entityManager;
    private TaskService $taskService;
    private BlendRepository $blendRepository;
    private ToolsService $toolsService;

    protected function setUp(): void {
        $this->entityManager = self::getContainer()->get(EntityManagerInterface::class);
        $this->blendRepository = self::getContainer()->get(BlendRepository::class);
        $this->taskService = self::getContainer()->get(TaskService::class);
        $this->toolsService = self::getContainer()->get(ToolsService::class);
    }

    public function testResetTask(): void {
        $blendId = $this->toolsService->addBlend("full", 10, 1, 1920, 1080);

        $blend = $this->blendRepository->find($blendId);
        $this->assertNotNull($blend);
        // add a task
        $task = new Task();
        $task->setBlend($blend);
        $task->setType(Task::TYPE_GENERATE_ZIP);
        $task->setStatus(Task::STATUS_RUNNING);
        $task->setStartTime((new \DateTime()));

        $this->entityManager->persist($task);
        $this->entityManager->flush();

        $this->assertTrue($this->taskService->reset($task));
        $this->entityManager->flush(); // flush is not part of the service

        $this->entityManager->refresh($task);

        $this->assertEquals(Task::STATUS_WAITING, $task->getStatus());
        $this->assertNull($task->getStartTime());
        $this->assertEquals(Task::TYPE_GENERATE_ZIP, $task->getType()); // reset a task should not change its type
    }
}
